package com.plans.rush.mediator.data.network.response.mapper

import com.plans.rush.mediator.data.db.model.ImageModel
import com.plans.rush.mediator.data.db.model.JointPostModel
import com.plans.rush.mediator.data.db.model.PostModel
import com.plans.rush.mediator.data.network.response.PostResp
import javax.inject.Inject

class PostRespModelMapper @Inject constructor(
    private val postTagRespModelMapper: PostTagRespModelMapper,
    private val imageRespModelMapper: ImageRespModelMapper
){
    //region Ordinal Mapper
    fun mapTo(item:PostResp, postType: Int): JointPostModel {
        val post = PostModel(
            id = item.id,
            mediaId = item.id,
            title = item.title,
            datetime = item.datetime,
            cover = item.cover,
            owner = item.owner,
            views = item.views,
            link = item.link,
            thumbDowns = item.thumbDowns,
            thumbUps = item.thumbUps,
            commentCount = item.commentCount,
            isAlbum = item.isAlbum,
            mediaCount = item.mediaCount,
            postType = postType
        )
        var images: List<ImageModel> = listOf()
        item.images?.let {
            images = imageRespModelMapper.mapTo(item.images, item.id)
        }
        val tag = postTagRespModelMapper.mapFrom(item.tags)
        return JointPostModel(
            post = post,
            images = images,
            tags = tag
        )
    }
    fun mapTo(list: List<PostResp>, postType: Int): List<JointPostModel> =
        list.map { mapTo(it, postType) }
    //endregion
}