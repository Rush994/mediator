package com.plans.rush.mediator.domain.interactor

import com.plans.rush.mediator.domain.usecase.LoadCommentsTopUseCase
import javax.inject.Inject

class CommentLoaders @Inject constructor(
    private val loadCommentsBestUseCase: LoadCommentsTopUseCase
) {
    fun topComments(postId: String) =
        loadCommentsBestUseCase(postId)
}