package com.plans.rush.mediator.data.network.response

import com.google.gson.annotations.SerializedName

class PostListByTagDataResp(
    @SerializedName("name")
    val name: String,
    @SerializedName("display_name")
    val displayName: String,
    @SerializedName("followers")
    val followers: Int,
    @SerializedName("total_items")
    val totalItems: Int,
    @SerializedName("following")
    val following: Boolean,
    @SerializedName("is_whitelisted")
    val isWhitelisted: Boolean,
    @SerializedName("background_hash")
    val backgroundHash: String,
    @SerializedName("background_is_animated")
    val backgroundIsAnimated: Boolean,
    @SerializedName("items")
    val items: List<PostResp>
)