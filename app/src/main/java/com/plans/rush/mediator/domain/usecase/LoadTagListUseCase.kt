package com.plans.rush.mediator.domain.usecase

import com.plans.rush.mediator.domain.entity.TagEntity
import com.plans.rush.mediator.domain.irepository.IPostsRepository
import io.reactivex.Single
import javax.inject.Inject

class LoadTagListUseCase @Inject constructor(
    private val postsRepository: IPostsRepository
) {
    operator fun invoke(): Single<List<TagEntity>> =
        postsRepository.loadTags()
}