package com.plans.rush.mediator.presentation.feature.postlist.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.plans.rush.mediator.R
import com.plans.rush.mediator.di.scope.ViewModelKey
import com.plans.rush.mediator.domain.interactor.PostLoaders
import com.plans.rush.mediator.domain.interactor.PostObservers
import com.plans.rush.mediator.presentation.feature.postlist.FragmentNavigationManager
import com.plans.rush.mediator.presentation.feature.postlist.PostListActivity
import com.plans.rush.mediator.presentation.feature.postlist.adapter.PostsDiffUtil
import com.plans.rush.mediator.presentation.feature.postlist.adapter.PostsRecyclerAdapter
import com.plans.rush.mediator.presentation.feature.postlist.viewmodel.PostListViewModel
import com.plans.rush.mediator.presentation.viewmodel.mapper.PostOuterEntityVMMapper
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap

@Module
class PostListModule {
    @Provides
    @IntoMap
    @ViewModelKey(PostListViewModel::class)
    fun provideViewModel(
        fragmentNavigator: FragmentNavigationManager): ViewModel =
        PostListViewModel(fragmentNavigator)

    @Provides
    fun providePostListViewModel(fragment: PostListActivity, factory: ViewModelProvider.Factory): PostListViewModel =
        ViewModelProviders.of(fragment, factory).get(PostListViewModel::class.java)

    @Provides
    fun provideSupportNavigator(activity: PostListActivity): FragmentNavigationManager =
        FragmentNavigationManager(activity.supportFragmentManager, R.id.container)
}