package com.plans.rush.mediator.presentation.base.tempobject

data class PrevSelectedObject(
    var discardedPosition: Int = -1,
    var clickedCounter: Int? = null
)