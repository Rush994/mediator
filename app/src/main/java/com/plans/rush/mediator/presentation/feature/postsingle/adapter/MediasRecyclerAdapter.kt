package com.plans.rush.mediator.presentation.feature.postsingle.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.plans.rush.mediator.R
import com.plans.rush.mediator.presentation.base.Constants
import com.plans.rush.mediator.presentation.base.types.MediaTypes
import com.plans.rush.mediator.presentation.feature.postsingle.adapter.itemholders.media.ImageItemHolder
import com.plans.rush.mediator.presentation.feature.postsingle.adapter.itemholders.media.VideoItemHolder
import com.plans.rush.mediator.presentation.viewmodel.ImageViewModel
import kotlinx.android.synthetic.main.activity_post_single.*

class MediasRecyclerAdapter: RecyclerView.Adapter<RecyclerView.ViewHolder>(){

    var medias: List<ImageViewModel> = mutableListOf()

    fun setMediaList(media: List<ImageViewModel>) {
        this.medias = media
        notifyDataSetChanged()
    }

    override fun getItemViewType(position: Int): Int {
        var type = MediaTypes.Image.value
        val item = medias.get(position)
        if (item.mp4 == null)
            type = MediaTypes.Image.value
        if(item.link.endsWith(".gif"))
            type = MediaTypes.Video.value
        if(item.link.endsWith(".mp4"))
            type = MediaTypes.Video.value
        return type
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder =
        when(viewType){
            MediaTypes.Image.value ->  ImageItemHolder(
                LayoutInflater.from(parent.context).inflate(
                    R.layout.media_image_item,
                    parent,
                    false
                )
            )
            MediaTypes.Video.value -> VideoItemHolder(
                LayoutInflater.from(parent.context).inflate(
                    R.layout.media_exoplayer_item,
                    parent,
                    false
                )
            )
            else -> TODO()
        }

    override fun getItemCount(): Int = medias.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = medias.get(position)
        when(getItemViewType(position)){
            MediaTypes.Image.value ->{
                holder as ImageItemHolder
                holder.bind(item, position)
            }
            MediaTypes.Video.value ->{
                holder as VideoItemHolder
                holder.bind(item, position)
            }
        }
    }

    override fun onViewDetachedFromWindow(holder: RecyclerView.ViewHolder) {
        super.onViewDetachedFromWindow(holder)
        when(getItemViewType(holder.layoutPosition)) {
            MediaTypes.Video.value -> {
                holder as VideoItemHolder
                holder.releasePlayer()
            }
        }
    }

    override fun onViewAttachedToWindow(holder: RecyclerView.ViewHolder) {
        super.onViewAttachedToWindow(holder)
        when(getItemViewType(holder.layoutPosition)) {
            MediaTypes.Video.value -> {
                holder as VideoItemHolder
                holder.initPlayer()
            }
        }
    }
}