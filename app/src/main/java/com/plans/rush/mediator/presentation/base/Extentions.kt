package com.plans.rush.mediator.presentation.base

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.util.Log
import android.view.View
import android.view.ViewAnimationUtils
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.core.content.FileProvider
import com.plans.rush.mediator.BuildConfig
import java.io.File

////////////////////////////////////////// Toast extension method
fun Context.toast(message: String) =
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
/////////////////////////////////////////////////////////////////

// logger
fun Context.logging(message: String, tag: String) =
    Log.d(tag, message)
///////////

//region Animation handling
fun Context.revealEditText(view: ConstraintLayout) {
    val cx = view.right - 30
    val cy = view.bottom - 60
    val finalRadius = Math.max(view.width, view.height)
    val anim = ViewAnimationUtils.createCircularReveal(view, cx, cy, 0f, finalRadius.toFloat())
    view.visibility = View.VISIBLE
    anim.start()
}

fun Context.hideEditText(view: ConstraintLayout) {
    val cx = view.right - 30
    val cy = view.bottom - 60
    val initialRadius = view.width
    val anim = ViewAnimationUtils.createCircularReveal(view, cx, cy, initialRadius.toFloat(), 0f)
    anim.addListener(object : AnimatorListenerAdapter() {
        override fun onAnimationEnd(animation: Animator) {
            super.onAnimationEnd(animation)
            view.visibility = View.INVISIBLE
        }
    })
    anim.start()
}
//endregion

fun Context.showNotificationOnDownloadFinish(filePath: String, isMp4: Boolean){
    val notificationManagerCompat = NotificationManagerCompat.from(this)
    val file = File(filePath)
    var appType = ""
    when(isMp4){
        true -> appType = "video/*"
        false -> appType = "image/*"
    }
    val notifyIntent = Intent() // TODO("Fire Gallery")
    notifyIntent.setAction(Intent.ACTION_VIEW)
    notifyIntent.type = appType
    notifyIntent.data = FileProvider.getUriForFile(this,
        BuildConfig.APPLICATION_ID + ".provider",
        file)
    val notifyPendingIntent = PendingIntent.getActivity(
        this,
        Constants.COMPLETE_NOTIFICATION_ID,
        notifyIntent,
        PendingIntent.FLAG_UPDATE_CURRENT
    )
    val nCompleteBuilder: NotificationCompat.Builder = NotificationCompat.Builder(this,"complete_channel")
    nCompleteBuilder.setContentIntent(notifyPendingIntent)
    nCompleteBuilder.setContentText("Media downloaded")
    nCompleteBuilder.setSmallIcon(android.R.drawable.stat_sys_download_done)
    notificationManagerCompat.notify(
        Constants.COMPLETE_NOTIFICATION_ID,
        nCompleteBuilder.build()
    )
}