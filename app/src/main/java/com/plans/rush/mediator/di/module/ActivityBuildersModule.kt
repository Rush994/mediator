package com.plans.rush.mediator.di.module

import com.plans.rush.mediator.presentation.feature.postlist.PostListActivity
import com.plans.rush.mediator.presentation.feature.postlist.di.PostListModule
import com.plans.rush.mediator.presentation.feature.postlistbytag.PostListByTagActivity
import com.plans.rush.mediator.presentation.feature.postlistbytag.di.PostListByTagModule
import com.plans.rush.mediator.presentation.feature.postsearchtype.PostSeachTypeActivity
import com.plans.rush.mediator.presentation.feature.postsingle.PostSingleActivity
import com.plans.rush.mediator.presentation.feature.postsingle.di.PostSingleModule
import com.plans.rush.mediator.presentation.feature.searchbytag.SearchByTagActivity
import com.plans.rush.mediator.presentation.feature.searchbytag.di.SearchByTagModule
import com.plans.rush.mediator.presentation.feature.searchbytitle.SearchByTitleActivity
import dagger.Module
import dagger.android.AndroidInjectionModule
import dagger.android.ContributesAndroidInjector

@Module(includes = arrayOf(AndroidInjectionModule::class))
abstract class ActivityBuildersModule {

    @ContributesAndroidInjector(modules = arrayOf(
        PostListModule::class,
        FragmentBuildersModule::class))
    abstract fun contributeBestPostListActivity(): PostListActivity

    @ContributesAndroidInjector(modules = arrayOf(
        PostSingleModule::class))
    abstract fun contributePostSingleActivity(): PostSingleActivity

    @ContributesAndroidInjector
    abstract fun contributePostSearchTypeActivity(): PostSeachTypeActivity

    @ContributesAndroidInjector(modules = arrayOf(
        SearchByTagModule::class))
    abstract fun contributeSearchByTagActivity(): SearchByTagActivity

    @ContributesAndroidInjector
    abstract fun contributeSearchByTitleActivity(): SearchByTitleActivity

    @ContributesAndroidInjector(modules = arrayOf(
        PostListByTagModule::class))
    abstract fun contributePostListByTagActivity(): PostListByTagActivity
}