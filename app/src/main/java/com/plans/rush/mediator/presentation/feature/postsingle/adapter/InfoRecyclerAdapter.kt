package com.plans.rush.mediator.presentation.feature.postsingle.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.plans.rush.mediator.R
import com.plans.rush.mediator.presentation.base.types.CellTypes
import com.plans.rush.mediator.presentation.feature.postsingle.adapter.itemholders.postsingle.CommentItemHolder
import com.plans.rush.mediator.presentation.feature.postsingle.adapter.itemholders.postsingle.InfoItemHolder
import com.plans.rush.mediator.presentation.feature.postsingle.adapter.itemholders.postsingle.TagRecyclerItemHolder
import com.plans.rush.mediator.presentation.feature.postsingle.adapter.itemholders.postsingle.TitleItemHolder
import com.plans.rush.mediator.presentation.viewmodel.CommentViewModel
import com.plans.rush.mediator.presentation.viewmodel.PostViewModel
import com.plans.rush.mediator.presentation.viewmodel.TagViewModel

class InfoRecyclerAdapter(
    val listener: (tag: String) -> Unit
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var post: PostViewModel = PostViewModel()
    private var comments: List<CommentViewModel> = mutableListOf()
    private var tags: List<TagViewModel> = mutableListOf()
    private val viewPool = RecyclerView.RecycledViewPool()

    fun setPost(post: PostViewModel) {
        this.post = post
    }

    fun setTagList(tags: List<TagViewModel>) {
        this.tags = tags
    }

    fun setCommentList(comments: List<CommentViewModel>) {
        this.comments = comments
        notifyDataSetChanged()
    }

    override fun getItemViewType(position: Int): Int =
        when (position) {
            0 -> CellTypes.PostTitle.value
            1 -> CellTypes.PostTag.value
            2 -> CellTypes.PostInfo.value
            else -> CellTypes.PostComment.value
        }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ) =
        when (viewType) {
            CellTypes.PostComment.value -> CommentItemHolder(
                LayoutInflater.from(parent.context).inflate(
                    R.layout.post_single_comment_item,
                    parent,
                    false
                )
            )
            CellTypes.PostTitle.value -> TitleItemHolder(
                LayoutInflater.from(parent.context).inflate(
                    R.layout.post_single_title_item,
                    parent,
                    false
                )
            )
            CellTypes.PostInfo.value -> InfoItemHolder(
                LayoutInflater.from(parent.context).inflate(
                    R.layout.post_single_info_item,
                    parent,
                    false
                )
            )
            CellTypes.PostTag.value -> TagRecyclerItemHolder(
                LayoutInflater.from(parent.context).inflate(
                    R.layout.post_single_tagrecycler_item,
                    parent,
                    false
                ), listener
            )
            else -> TODO()
        }

    override fun getItemCount(): Int = comments.size + 3

    override fun onBindViewHolder(
        holder: RecyclerView.ViewHolder,
        position: Int
    ) {
        when (getItemViewType(position)) {
            CellTypes.PostTitle.value -> {
                holder as TitleItemHolder
                holder.bind(post.title)
            }
            CellTypes.PostTag.value -> {
                holder as TagRecyclerItemHolder
                holder.bind(tags, viewPool)
            }
            CellTypes.PostInfo.value -> {
                holder as InfoItemHolder
                holder.bind(post)
            }
            CellTypes.PostComment.value -> {
                holder as CommentItemHolder
                val item = comments[position - 3]
                holder.bind(item)
            }
        }
    }
}