package com.plans.rush.mediator.presentation.feature.searchbytag.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.plans.rush.mediator.R
import com.plans.rush.mediator.presentation.base.tempobject.PrevSelectedObject
import com.plans.rush.mediator.presentation.feature.searchbytag.listener.OnTagClickListener
import com.plans.rush.mediator.presentation.viewmodel.TagViewModel
import kotlinx.android.synthetic.main.tag_list_item.view.*

class TagsRecyclerAdapter(val listener: OnTagClickListener): RecyclerView.Adapter<TagsRecyclerAdapter.SingleItemHolder>() {
    private var tags: List<TagViewModel> = mutableListOf()

    fun setTagList(tags: List<TagViewModel>) {
        this.tags = tags
        notifyDataSetChanged()
    }

    fun findCheckedItems(): ArrayList<String>{
        val tagStrings: ArrayList<String> = arrayListOf()
        for (item in tags)
            if(item.isChecked)
                tagStrings.add(item.name)
        return tagStrings
    }

    fun discardSelection(clickedCounter: Int): PrevSelectedObject{
        val prevSelectedObject = PrevSelectedObject()
        for(i in 0 until tags.size)
            if(tags[i].isChecked) {
                tags[i].isChecked = false
                prevSelectedObject.discardedPosition = i
                prevSelectedObject.clickedCounter = clickedCounter - 1
                notifyItemChanged(i)
            }
        return prevSelectedObject
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ) =
        SingleItemHolder(LayoutInflater.from(parent.context).inflate(R.layout.tag_list_item, parent, false))

    override fun getItemCount(): Int = tags.size

    override fun onBindViewHolder(
        holder: SingleItemHolder,
        position: Int
    ) {
        val item = tags[position]
        holder.bind(item, position)
    }

    inner class SingleItemHolder(private val view: View): RecyclerView.ViewHolder(view){
        fun bind(item: TagViewModel, position: Int) {
            with(view){
                chipTagItem.text = item.displayName
                chipTagItem.isChecked = item.isChecked
                setOnClickListener {
                    listener.onItemClick(item, position)
                }
            }
        }
    }
}