package com.plans.rush.mediator.presentation.viewmodel.mapper

import com.plans.rush.mediator.domain.common.ListMapper
import com.plans.rush.mediator.domain.entity.ImageEntity
import com.plans.rush.mediator.presentation.viewmodel.ImageViewModel
import javax.inject.Inject

class ImageEntityVMMapper @Inject constructor():
    ListMapper<ImageEntity, ImageViewModel> {
    override fun mapFrom(item: ImageEntity): ImageViewModel =
        ImageViewModel(
            postId = item.postId,
            mediaId = item.mediaId,
            title = item.title,
            description = item.description,
            datetime = item.datetime,
            mp4 = item.mp4,
            link = item.link
        )
    override fun mapFrom(list: List<ImageEntity>): List<ImageViewModel> =
        list.map { mapFrom(it) }
}