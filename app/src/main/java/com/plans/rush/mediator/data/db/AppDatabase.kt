package com.plans.rush.mediator.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.plans.rush.mediator.data.db.dao.*
import com.plans.rush.mediator.data.db.model.*

@Database(entities = [
    PostModel::class,
    ImageModel::class,
    CommentModel::class,
    TagModel::class,
    PostTagJoinModel::class],
    version = 1,
    exportSchema = false)
abstract class AppDatabase: RoomDatabase() {
    companion object{
        const val TABLE_POSTS_TOP = "PostsTop"
        const val TABLE_IMAGES_TOP = "ImagesTop"
        const val TABLE_COMMENTS_TOP = "CommentsTop"
        const val TABLE_POST_TAG_JOINS_TOP = "PostTagJoinsTop"

        const val TABLE_POST_TAGS = "PostTags"
    }

    abstract fun getPostsOutersDao(): PostsDao

    abstract fun getPostInnersDao(): ImagesDao

    abstract fun getCommentsDao(): CommentsDao

    abstract fun getPostTagsDao(): TagsDao

    abstract fun getPostTagJoinsDao(): PostTagJoinsDao
}