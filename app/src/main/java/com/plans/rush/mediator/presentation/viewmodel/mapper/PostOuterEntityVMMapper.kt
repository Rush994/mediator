package com.plans.rush.mediator.presentation.viewmodel.mapper

import com.plans.rush.mediator.domain.entity.PostEntity
import com.plans.rush.mediator.domain.common.ListMapper
import com.plans.rush.mediator.presentation.viewmodel.PostViewModel
import javax.inject.Inject

class PostOuterEntityVMMapper @Inject constructor(
    private val tagEntityVMMapper: TagEntityVMMapper
):
    ListMapper<PostEntity, PostViewModel> {
    override fun mapFrom(item: PostEntity): PostViewModel =
        PostViewModel(
            id = item.id,
            mediaId = item.mediaId,
            title = item.title,
            datetime = item.datetime,
            cover = item.cover,
            owner = item.owner,
            views = item.views,
            link = item.link,
            thumbUps = item.thumbUps,
            thumbDowns = item.thumbDowns,
            commentCount = item.commentCount,
            isAlbum = item.isAlbum,
            mediaCount = item.mediaCount,
            categoryType = item.categoryType
        )

    override fun mapFrom(list: List<PostEntity>): List<PostViewModel> =
        list.map { mapFrom(it) }
}