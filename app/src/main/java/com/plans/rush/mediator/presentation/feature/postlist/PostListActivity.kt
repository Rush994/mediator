package com.plans.rush.mediator.presentation.feature.postlist

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.lifecycle.Observer
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.plans.rush.mediator.R
import com.plans.rush.mediator.presentation.base.BaseActivity
import com.plans.rush.mediator.presentation.base.types.PageTypes
import com.plans.rush.mediator.presentation.feature.postlist.adapter.PostsDiffUtil
import com.plans.rush.mediator.presentation.feature.postlist.listener.OnPostClickListener
import com.plans.rush.mediator.presentation.feature.postlist.viewmodel.PostListViewModel
import com.plans.rush.mediator.presentation.viewmodel.PostViewModel
import kotlinx.android.synthetic.main.activity_post_list.*
import javax.inject.Inject


class PostListActivity : BaseActivity(), OnPostClickListener {

    @Inject
    lateinit var viewModel: PostListViewModel

    override fun onCreate(
        savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_post_list)

        bottonNavView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        viewModel.titleChangeLiveData.observe(
            this,
            Observer {
                supportActionBar?.title = it
            }
        )
        viewModel.openTopTab(getString(R.string.actionBarTitleTop))
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.action_bar_list_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean =
        when (item.itemId) {
            R.id.action_search -> {
                fireActivity(PageTypes.PreSearch)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }

    override fun onItemClick(item: PostViewModel) {
        fireActivity(PageTypes.PostSingle, item)
    }

    private val mOnNavigationItemSelectedListener =
        BottomNavigationView.OnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                R.id.nav_best -> return@OnNavigationItemSelectedListener viewModel.openTopTab(getString(R.string.actionBarTitleTop))
                R.id.nav_cats -> return@OnNavigationItemSelectedListener viewModel.openCatTab(getString(R.string.actionBarTitleCats))
                R.id.nav_dogs -> return@OnNavigationItemSelectedListener viewModel.openDogTab(getString(R.string.actionBarTitleDogs))
            }
            false
        }
}
