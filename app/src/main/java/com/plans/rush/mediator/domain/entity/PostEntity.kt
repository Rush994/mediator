package com.plans.rush.mediator.domain.entity

import com.plans.rush.mediator.presentation.base.types.CategoryTypes

data class PostEntity(
    val id: String = "",
    val mediaId: String = "",
    val title: String = "",
    val datetime: Long = 0,
    val cover: String? = null,
    val owner: String = "",
    val views: Int = 0,
    val link: String = "",
    val thumbUps: Int = 0,
    val thumbDowns: Int = 0,
    val commentCount: Int = 0,
    val isAlbum: Boolean = false,
    var mediaCount: Int? = null,
    val categoryType: CategoryTypes = CategoryTypes.Default,
    val images: List<ImageEntity> = listOf()
)