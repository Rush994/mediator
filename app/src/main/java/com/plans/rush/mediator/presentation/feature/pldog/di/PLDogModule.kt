package com.plans.rush.mediator.presentation.feature.pldog.di

import androidx.lifecycle.ViewModel
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.plans.rush.mediator.di.scope.ViewModelKey
import com.plans.rush.mediator.domain.interactor.PostLoaders
import com.plans.rush.mediator.domain.interactor.PostObservers
import com.plans.rush.mediator.presentation.feature.pldog.viewmodel.PLDogViewModel
import com.plans.rush.mediator.presentation.feature.pltop.viewmodel.PLTopViewModel
import com.plans.rush.mediator.presentation.feature.postlist.PostListActivity
import com.plans.rush.mediator.presentation.feature.postlist.adapter.PostsDiffUtil
import com.plans.rush.mediator.presentation.feature.postlist.adapter.PostsRecyclerAdapter
import com.plans.rush.mediator.presentation.viewmodel.mapper.PostOuterEntityVMMapper
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap

@Module
class PLDogModule{
    @Provides
    @IntoMap
    @ViewModelKey(PLDogViewModel::class)
    fun provideViewModel(postLoaders: PostLoaders,
                         postObservers: PostObservers,
                         postOuterEntityVMMapper: PostOuterEntityVMMapper
    ): ViewModel =
        PLDogViewModel(postLoaders, postObservers, postOuterEntityVMMapper)

    @Provides
    fun providePostsAdapter(activity: PostListActivity): PostsRecyclerAdapter =
        PostsRecyclerAdapter(activity)

    @Provides
    fun provideStaggeredManager(): StaggeredGridLayoutManager =
        StaggeredGridLayoutManager(2, RecyclerView.VERTICAL)

    @Provides
    fun providePostsDiffUtil(): PostsDiffUtil =
        PostsDiffUtil()

    @Provides
    fun providePostsDiffResult(postsDiffUtil: PostsDiffUtil): DiffUtil.DiffResult =
        DiffUtil.calculateDiff(postsDiffUtil)
}