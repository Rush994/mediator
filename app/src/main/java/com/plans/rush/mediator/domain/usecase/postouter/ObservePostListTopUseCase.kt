package com.plans.rush.mediator.domain.usecase.postouter

import com.plans.rush.mediator.domain.entity.PostEntity
import com.plans.rush.mediator.domain.irepository.IPostsRepository
import io.reactivex.Flowable
import javax.inject.Inject

class ObservePostListTopUseCase @Inject constructor(
    private val postsRepository: IPostsRepository
){
    operator fun invoke(postType: Int): Flowable<List<PostEntity>> =
        postsRepository.observePosts(postType)
}