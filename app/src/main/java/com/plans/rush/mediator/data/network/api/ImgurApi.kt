package com.plans.rush.mediator.data.network.api

import com.plans.rush.mediator.data.network.api.response.*
import com.plans.rush.mediator.data.network.response.PostResp
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path

interface ImgurApi {
    companion object{
        const val BASE_URL = "https://api.imgur.com/"
        const val API_KEY = "046823c07ef6628"
    }

    @GET("3/gallery/top/{page}?showViral=false")
    fun getBestPostList(@Path("page") page: Int): Single<PostListResp>

    @GET("3/gallery/t/{tag}/{page}")
    fun getPostListByTag(@Path("tag") tag: String,
                         @Path("page") page: Int): Single<PostListByTagResp>

    @GET("3/gallery/album/{galleryHash}")
    fun getPostInfoInner(@Path("galleryHash") postId: String): Single<PostSingleResp>

    @GET("3/image/{imageHash}")
    fun getImageInfo(@Path("imageHash") imageId: String): Single<PostSingleResp>

    @GET("3/gallery/{imageHash}/comments/best")
    fun getPostCommentList(@Path("imageHash") postId: String): Single<CommentListResp>

    @GET("3/tags")
    fun getTagList(): Single<TagListDataResp>
}