package com.plans.rush.mediator.data.db.dao

import androidx.room.Dao
import androidx.room.Query
import com.plans.rush.mediator.data.db.AppDatabase
import com.plans.rush.mediator.data.db.model.ImageModel
import io.reactivex.Flowable
import io.reactivex.Observable

@Dao
abstract class ImagesDao: BaseDao<ImageModel>(AppDatabase.TABLE_IMAGES_TOP) {
    @Query("SELECT * FROM ImagesTop WHERE PostId LIKE :postId")
    abstract fun observePostSingle(postId: String): Flowable<List<ImageModel>>
}