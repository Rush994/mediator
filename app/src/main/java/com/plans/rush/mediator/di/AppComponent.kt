package com.plans.rush.mediator.di

import android.content.Context
import com.plans.rush.mediator.App
import com.plans.rush.mediator.di.module.*
import com.plans.rush.mediator.di.scope.PerApplication
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector

@PerApplication
@Component(modules = [AppModule::class, ActivityBuildersModule::class, ViewModelFactoryModule::class, NetworkModule::class, RoomModule::class, DataModule::class])
interface AppComponent: AndroidInjector<App> {
    @Component.Builder
    interface Builder {
        @BindsInstance
        fun context(mContext: Context): Builder

        fun build(): AppComponent
    }
}