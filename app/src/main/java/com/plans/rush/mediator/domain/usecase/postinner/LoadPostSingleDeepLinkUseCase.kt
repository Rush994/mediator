package com.plans.rush.mediator.domain.usecase.postinner

import com.plans.rush.mediator.domain.irepository.IPostsRepository
import com.plans.rush.mediator.presentation.base.types.CategoryTypes
import javax.inject.Inject

class LoadPostSingleDeepLinkUseCase @Inject constructor(
    private val postsRepository: IPostsRepository
) {
    operator fun invoke(postId:String, categoryTypes: Int) =
        postsRepository.loadPost(postId, categoryTypes)
}