package com.plans.rush.mediator.domain.usecase.postouter

import com.plans.rush.mediator.domain.irepository.IPostsRepository
import javax.inject.Inject

class ObservePostUseCase @Inject constructor(
    private val postsRepository: IPostsRepository
) {
    operator fun invoke(postId: String) =
        postsRepository.findPost(postId)
}