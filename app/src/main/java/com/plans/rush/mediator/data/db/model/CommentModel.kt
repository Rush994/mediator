package com.plans.rush.mediator.data.db.model

import androidx.room.*
import com.plans.rush.mediator.data.db.AppDatabase

@Entity(tableName = AppDatabase.TABLE_COMMENTS_TOP,
    foreignKeys = [
        ForeignKey(
            entity = PostModel::class,
            parentColumns = ["Id"],
            childColumns = ["PostId"],
            onDelete = ForeignKey.CASCADE)])
data class CommentModel(
    @PrimaryKey
    @ColumnInfo(name="Id", index = true)
    val id: Long,
    @ColumnInfo(name = "ImageId")
    val imageId: String,
    @ColumnInfo(name = "Comment")
    val comment: String,
    @ColumnInfo(name = "Author")
    val author: String,
    @ColumnInfo(name = "Ups")
    val ups: Long,
    @ColumnInfo(name = "Downs")
    val downs: Long,
    @ColumnInfo(name = "Platform")
    val platform: String,

    @ColumnInfo(name = "PostId", index = true)
    val postId: String
)