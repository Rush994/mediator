package com.plans.rush.mediator.data.network.interceptor

import com.plans.rush.mediator.data.network.api.ImgurApi
import okhttp3.Interceptor
import okhttp3.Response
import javax.inject.Inject

class RequestInterceptor @Inject constructor(): Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        var request = chain.request()
        request = request.newBuilder()
            .addHeader("Authorization", "Client-ID ${ImgurApi.API_KEY}")
            .build()

        return chain.proceed(request)
    }
}