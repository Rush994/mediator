package com.plans.rush.mediator.presentation.feature.pltop

import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.plans.rush.mediator.R
import com.plans.rush.mediator.presentation.base.BaseFragment
import com.plans.rush.mediator.presentation.base.Constants
import com.plans.rush.mediator.presentation.base.logging
import com.plans.rush.mediator.presentation.base.types.CategoryTypes
import com.plans.rush.mediator.presentation.feature.pltop.viewmodel.PLTopViewModel
import com.plans.rush.mediator.presentation.feature.postlist.adapter.PostsDiffUtil
import com.plans.rush.mediator.presentation.feature.postlist.adapter.PostsRecyclerAdapter
import kotlinx.android.synthetic.main.fragment_pltop.*
import javax.inject.Inject

class PLTopFragment : BaseFragment() {

    companion object {
        fun newInstance(): Fragment = PLTopFragment()
    }

    @Inject
    lateinit var viewModel: PLTopViewModel
    @Inject
    lateinit var postsDiffUtil: PostsDiffUtil
    @Inject
    lateinit var postsDiffResult: DiffUtil.DiffResult
    @Inject
    lateinit var postsAdapter: PostsRecyclerAdapter
    @Inject
    lateinit var gridLayoutManager: StaggeredGridLayoutManager
    private var lastVisibleItem: Int = 0

    override fun layoutRes(): Int = R.layout.fragment_pltop

    override fun initViews() {
        srlTopPosts.setOnRefreshListener {
            viewModel.lastLoadedPage = 0
            viewModel.loadPosts()
        }
        fabTopUpward.setOnClickListener {
            rvTopPosts.smoothScrollToPosition(0)
        }
        rvTopPosts.setOnScrollChangeListener { _, _, _, _, _ ->
            var item = IntArray(2)
            item = gridLayoutManager.findLastVisibleItemPositions(item)
            lastVisibleItem = item[0]
            showUpwardFab()
            showNextListPortion()
        }
        rvTopPosts.layoutManager = gridLayoutManager
        rvTopPosts.adapter = postsAdapter
    }

    override fun initDisplayableList() {
        currentCategoryType = CategoryTypes.Ordinal
        viewModel.postListLiveData.observe(
            this,
            Observer { list ->
                if (postsAdapter.posts.isEmpty()) {
                    postsAdapter.posts = list
                    postsAdapter.notifyDataSetChanged()
                }
                else
                {
                    postsDiffUtil.oldList = postsAdapter.posts
                    postsDiffUtil.newList = list
                    postsDiffResult = DiffUtil.calculateDiff(postsDiffUtil)
                    postsAdapter.posts = list
                    postsDiffResult.dispatchUpdatesTo(postsAdapter)
                }
                srlTopPosts.isRefreshing = false
            }
        )
        viewModel.progressStateLiveData.observe(
            this,
            Observer {
                it.let {
                    if (it.progressVisible) {
                        pbTopPosts.visibility = View.VISIBLE
                        rvTopPosts.visibility = View.GONE
                    } else {
                        pbTopPosts.visibility = View.GONE
                        rvTopPosts.visibility = View.VISIBLE
                    }
                }
            }
        )

        viewModel.postListInit(currentCategoryType)
    }

    private fun showUpwardFab() {
        if (lastVisibleItem > Constants.THRESHOLD_RECYCLER_VISIBLE_ITEMS)
            fabTopUpward.visibility = View.VISIBLE
        else fabTopUpward.visibility = View.GONE
    }

    private fun showNextListPortion() {
        val totalItemCount = gridLayoutManager.itemCount
        if (!viewModel.loading && totalItemCount <= (lastVisibleItem + Constants.THRESHOLD_RECYCLER_LOADER)) {
            viewModel.loading = true
            activity?.logging(
                "Load new list of ${currentCategoryType.value}",
                Constants.LOADING_POSTS
            )
            viewModel.loadPosts()
        }
    }
}
