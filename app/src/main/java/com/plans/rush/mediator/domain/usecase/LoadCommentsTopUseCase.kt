package com.plans.rush.mediator.domain.usecase

import com.plans.rush.mediator.domain.irepository.IPostsRepository
import io.reactivex.Completable
import javax.inject.Inject

class LoadCommentsTopUseCase @Inject constructor(
    private val postsRepository: IPostsRepository
) {
    operator fun invoke(postId: String): Completable =
        postsRepository.loadComments(postId)
}