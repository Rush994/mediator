package com.plans.rush.mediator.data.db.model

import androidx.room.*
import com.plans.rush.mediator.data.db.AppDatabase

@Entity(tableName = AppDatabase.TABLE_IMAGES_TOP,
        foreignKeys = [
        ForeignKey(
            entity = PostModel::class,
            parentColumns = ["Id"],
            childColumns = ["PostId"],
            onDelete = ForeignKey.CASCADE)],
    indices = arrayOf(Index("MediaId")))
data class ImageModel(
    @ColumnInfo(name = "PostId", index = true)
    val postId: String,
    @PrimaryKey
    @ColumnInfo(name = "MediaId")
    val mediaId: String,
    @ColumnInfo(name = "Title")
    val title: String?,
    @ColumnInfo(name ="Description")
    val description: String?,
    @ColumnInfo(name = "DateTime")
    val datetime: Long,
    @ColumnInfo(name = "Mp4")
    val mp4: String?,
    @ColumnInfo(name = "Link")
    val link: String
)