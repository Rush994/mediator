package com.plans.rush.mediator.presentation.feature.postsingle.adapter.itemholders.postsingle

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.post_single_title_item.view.*

class TitleItemHolder(private val view: View): RecyclerView.ViewHolder(view) {
    fun bind(item: String) {
        with(view) {
            tvPostTitle.text = item
        }
    }
}