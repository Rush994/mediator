package com.plans.rush.mediator.presentation.base.types

enum class PageTypes(val value: Int) {
    None(0), PostList(1),
    PostSingle(2), PreSearch(3),
    SearchByTag(4)
}