package com.plans.rush.mediator.di.module

import com.plans.rush.mediator.presentation.feature.plcat.PLCatFragment
import com.plans.rush.mediator.presentation.feature.plcat.di.PLCatModule
import com.plans.rush.mediator.presentation.feature.pldog.PLDogFragment
import com.plans.rush.mediator.presentation.feature.pldog.di.PLDogModule
import com.plans.rush.mediator.presentation.feature.pltop.PLTopFragment
import com.plans.rush.mediator.presentation.feature.pltop.di.PLTopModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentBuildersModule {
    @ContributesAndroidInjector(modules = arrayOf(
        PLTopModule::class
    ))
    abstract fun contributePLTopFragment(): PLTopFragment

    @ContributesAndroidInjector(modules = arrayOf(
        PLCatModule::class
    ))
    abstract fun contributePLCatFragment(): PLCatFragment

    @ContributesAndroidInjector(modules = arrayOf(
        PLDogModule::class))
    abstract fun contributePLDogFragment(): PLDogFragment
}