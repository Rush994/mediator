package com.plans.rush.mediator.presentation.viewmodel.mapper

import com.plans.rush.mediator.domain.common.ListMapper
import com.plans.rush.mediator.domain.entity.CommentEntity
import com.plans.rush.mediator.presentation.viewmodel.CommentViewModel
import javax.inject.Inject

class CommentEntityVMMapper @Inject constructor(): ListMapper<CommentEntity, CommentViewModel>{
    override fun mapFrom(item: CommentEntity): CommentViewModel =
        CommentViewModel(
            id = item.id,
            imageId = item.imageId,
            comment = item.comment,
            author = item.author,
            ups = item.ups,
            downs = item.downs,
            platform = item.platform
        )

    override fun mapFrom(list: List<CommentEntity>): List<CommentViewModel> =
        list.map { mapFrom(it) }

}