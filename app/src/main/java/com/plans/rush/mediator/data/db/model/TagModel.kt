package com.plans.rush.mediator.data.db.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.plans.rush.mediator.data.db.AppDatabase

@Entity(tableName = AppDatabase.TABLE_POST_TAGS)
data class TagModel(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "Id")
    val id: Long = 0,
    @ColumnInfo(name = "Name")
    val name: String,
    @ColumnInfo(name = "DisplayName")
    val displayName: String,
    @ColumnInfo(name = "Followers")
    val followers: Int,
    @ColumnInfo(name = "TotalItems")
    val totalItems: Int
)