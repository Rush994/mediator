package com.plans.rush.mediator.presentation.base

import androidx.appcompat.app.AppCompatActivity
import com.plans.rush.mediator.presentation.base.types.PageTypes
import com.plans.rush.mediator.presentation.viewmodel.PostViewModel
import dagger.android.support.DaggerAppCompatActivity
import javax.inject.Inject

open class BaseActivity: DaggerAppCompatActivity() {

    @Inject
    lateinit var activityNavigationManager: ActivityNavigationManager

    fun fireActivity(destination: PageTypes,
                     postData: PostViewModel = PostViewModel(),
                     tags: ArrayList<String> = arrayListOf()) {
        activityNavigationManager.Navigate(destination, this, postData, tags)
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
    }

    private fun extinguishActivity(activity: AppCompatActivity){
        activity.finish()
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
    }

    override fun onBackPressed() {
        extinguishActivity(this)
    }
}