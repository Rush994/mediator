package com.plans.rush.mediator.data.network.response.mapper

import com.plans.rush.mediator.data.db.model.ImageModel
import com.plans.rush.mediator.data.network.response.ImageResp
import javax.inject.Inject

class ImageRespModelMapper @Inject constructor() {
    //region Ordinal Mapper
    private fun mapTo(item: ImageResp, postId: String): ImageModel =
        ImageModel(
            postId = postId,
            mediaId = item.id,
            title = item.title,
            description = item.description,
            datetime = item.datetime,
            mp4 = item.mp4,
            link = item.link
        )
    fun mapTo(list: List<ImageResp>, postId: String): List<ImageModel> =
        list.map { mapTo(it, postId) }
    //endregion
}