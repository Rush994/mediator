package com.plans.rush.mediator.presentation.feature.postsingle.adapter.itemholders.media

import android.net.Uri
import android.util.Log
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.google.android.exoplayer2.ExoPlayerFactory
import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory
import com.google.android.exoplayer2.source.ExtractorMediaSource
import com.google.android.exoplayer2.source.LoopingMediaSource
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout
import com.google.android.exoplayer2.ui.PlayerView
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.util.Util
import com.plans.rush.mediator.R
import com.plans.rush.mediator.presentation.base.Constants
import com.plans.rush.mediator.presentation.base.types.ContextMenuItemTypes
import com.plans.rush.mediator.presentation.viewmodel.ImageViewModel
import kotlinx.android.synthetic.main.media_exoplayer_item.view.*
import java.util.*

class VideoItemHolder(private val view: View) : RecyclerView.ViewHolder(view) {
    lateinit var simpleExoplayer: SimpleExoPlayer
    var pausedOnCollapse = false
    var isVideoAdded = false

    private val bandwidthMeter by lazy {
        DefaultBandwidthMeter()
    }
    private val extractorsFactory by lazy {
        DefaultExtractorsFactory()
    }
    var isMute: Boolean = true
    private var uriLink = Uri.EMPTY

    fun bind(item: ImageViewModel, position: Int){
        uriLink = Uri.parse("https://i.imgur.com/${item.mediaId}.mp4")
        item.link = "https://i.imgur.com/${item.mediaId}.mp4"
        with(view){
            fabVolume.setOnClickListener {
                if (isMute) {
                    unMuteVideo()
                    fabVolume.setImageResource(R.drawable.ic_volume_up)
                } else {
                    muteVideo()
                    fabVolume.setImageResource(R.drawable.ic_volume_off)
                }
                isMute = isMute.not()
            }

            videoDescription.text = item.title
            setOnCreateContextMenuListener { contextMenu, view, contextMenuInfo ->
                contextMenu.add(position, ContextMenuItemTypes.Download.value,
                    0, R.string.postInfoDownload)
                contextMenu.add(position, ContextMenuItemTypes.Share.value,
                    0, R.string.postInfoShare)
            }
            simpleExoPlayerView.useController = false
            simpleExoPlayerView.resizeMode = AspectRatioFrameLayout.RESIZE_MODE_ZOOM
        }
    }

    private fun initializeExoplayer() {
        val videoTrackSelectionFactory = AdaptiveTrackSelection.Factory(bandwidthMeter)
        val trackSelector = DefaultTrackSelector(videoTrackSelectionFactory)
        simpleExoplayer = ExoPlayerFactory.newSimpleInstance(view.context, trackSelector)
    }
    fun initPlayer(){
        initializeExoplayer()
        view.simpleExoPlayerView.player = simpleExoplayer
        setPlayerUri()
        muteVideo(); isMute = true
        simpleExoplayer.addListener(object : Player.EventListener{
            override fun onPlayerStateChanged(playWhenReady: Boolean, playbackState: Int) {
                super.onPlayerStateChanged(playWhenReady, playbackState)
                when (playbackState) {
                    Player.STATE_BUFFERING -> {
                        Log.e(Constants.RECYCLER_EXO_TAG, "onPlayerStateChanged: Buffering video.")
                    }
                    Player.STATE_ENDED -> {
                        Log.d(Constants.RECYCLER_EXO_TAG, "onPlayerStateChanged: Video ended.")
                    }
                    Player.STATE_IDLE -> {
                    }
                    Player.STATE_READY -> {
                        Log.e(Constants.RECYCLER_EXO_TAG, "onPlayerStateChanged: Ready to play.")
                    }
                    else -> {
                    }
                }
            }
        })
    }

    private fun setPlayerUri() {
        // Produces DataSource instances through which medias data is loaded.
        val dataSourceFactory = DefaultDataSourceFactory(
            view.context, Util.getUserAgent(
                view.context,
                view.context.getString(R.string.app_name)
            )
        )
        val videoSource = ExtractorMediaSource(
            uriLink,
            dataSourceFactory, extractorsFactory, null, null
        )

        val loopingSource = LoopingMediaSource(videoSource)
        isVideoAdded = true
        simpleExoplayer.prepare(loopingSource)
        resumeVideo(true)
    }
    private fun muteVideo(){
        simpleExoplayer.volume = 0f
    }
    private fun unMuteVideo(){
        simpleExoplayer.volume = 1f
    }
    fun resumeVideo(isScroll: Boolean = false){
        if (!pausedOnCollapse && !isScroll)
            simpleExoplayer.seekTo(0)
        simpleExoplayer.playWhenReady = true
        with(view){
            simpleExoPlayerView.visibility = View.VISIBLE
            fabVolume.visibility = View.VISIBLE
            coverExoPlayer.visibility = View.GONE
        }
    }
    fun pauseVideo(pausedOnCollapse: Boolean = false){
        this.pausedOnCollapse = pausedOnCollapse
        simpleExoplayer.playWhenReady = false
    }

    fun releasePlayer() {
        simpleExoplayer.release()
    }
}