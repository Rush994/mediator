package com.plans.rush.mediator.data.db.source

import com.plans.rush.mediator.data.db.dao.*
import com.plans.rush.mediator.data.db.model.*
import com.plans.rush.mediator.presentation.base.types.CategoryTypes
import io.reactivex.Completable
import io.reactivex.Flowable
import javax.inject.Inject

class PostsDBSource @Inject constructor(
    private val postsDao: PostsDao,
    private val tagsDao: TagsDao,
    private val postTagJoinsDao: PostTagJoinsDao,
    private val imagesDao: ImagesDao,
    private val commentsDao: CommentsDao
) : IPostsDBSource {

    override fun observePosts(postType: Int): Flowable<List<PostModel>> =
        postsDao.observeBestPostList(postType)


    override fun observeTags(postId: String): Flowable<List<TagModel>> =
        postTagJoinsDao.observePostTags(postId)

    override fun insertPosts(jointPosts: List<JointPostModel>): Completable =
        Completable.fromAction {
            val posts: MutableList<PostModel> = mutableListOf()
            val tags: MutableList<TagModel> = mutableListOf()
            val postTagJoins: MutableList<PostTagJoinModel> = mutableListOf()

            jointPosts.forEach {
                if (it.post.isAlbum) {
                    it.post.mediaId = it.post.cover!!
                }
                posts.add(it.post)
                tags.addAll(it.tags)

                for (item in it.tags)
                    postTagJoins.add(
                        PostTagJoinModel(
                            it.post.id,
                            0
                        )
                    )
            }
            postsDao.insertOrReplace(posts)
            val ids = tagsDao.insertOrReplace(tags)
            val map = postTagJoins.zip(ids).toList()
            postTagJoins.clear()
            for (item in map) {
                item.first.tagId = item.second
                postTagJoins.add(item.first)
            }
            postTagJoinsDao.insertOrReplace(postTagJoins)
            if ((jointPosts.size == 1) && (jointPosts[0].post.postType == CategoryTypes.DeepLink.value))
                imagesDao.insertOrReplace(jointPosts[0].images)
        }

    override fun observePost(postId: String): Flowable<PostModel> =
        postsDao.findPost(postId)


    override fun observeImages(postId: String): Flowable<List<ImageModel>> =
        imagesDao.observePostSingle(postId)


    override fun insertImage(image: ImageModel): Completable =
        Completable.fromAction {
            imagesDao.insertOrReplace(image)
        }

    override fun insertImages(jointPost: JointPostModel): Completable =
        Completable.fromAction{
            if (jointPost.images.isNotEmpty()) {
                imagesDao.insertOrReplace(jointPost.images)
            }
            else insertImage(
                ImageModel(
                    jointPost.post.id, jointPost.post.mediaId, jointPost.post.title, null,
                    jointPost.post.datetime, null, jointPost.post.link
                )
            )
        }

    override fun observeComments(postId: String): Flowable<List<CommentModel>> =
        commentsDao.observeCommentList(postId)

    override fun insertComments(commentList: List<CommentModel>): Completable =
        Completable.fromAction {
            commentsDao.insertOrReplace(commentList)
        }

    override fun deletePosts(postType: Int): Completable =
        Completable.fromAction {
            postsDao.deletePostList(postType)
        }

    override fun deletePostsByTag(): Completable =
        Completable.fromAction {
            postsDao.deletePostList(CategoryTypes.ByTag.value)
        }
}