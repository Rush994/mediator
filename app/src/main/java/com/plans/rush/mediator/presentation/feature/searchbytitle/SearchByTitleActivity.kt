package com.plans.rush.mediator.presentation.feature.searchbytitle

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.plans.rush.mediator.R

class SearchByTitleActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search_by_title)
    }
}
