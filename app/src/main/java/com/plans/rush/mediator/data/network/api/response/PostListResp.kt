package com.plans.rush.mediator.data.network.api.response

import com.google.gson.annotations.SerializedName
import com.plans.rush.mediator.data.network.response.PostResp

data class PostListResp(
    @SerializedName("data")
    val data: List<PostResp>,
    @SerializedName("success")
    val success: Boolean,
    @SerializedName("status")
    private var status: Int
)