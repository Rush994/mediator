package com.plans.rush.mediator.presentation.feature.pldog

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.plans.rush.mediator.R
import com.plans.rush.mediator.presentation.base.BaseFragment
import com.plans.rush.mediator.presentation.base.Constants
import com.plans.rush.mediator.presentation.base.logging
import com.plans.rush.mediator.presentation.base.types.CategoryTypes
import com.plans.rush.mediator.presentation.feature.pldog.viewmodel.PLDogViewModel
import com.plans.rush.mediator.presentation.feature.postlist.adapter.PostsDiffUtil
import com.plans.rush.mediator.presentation.feature.postlist.adapter.PostsRecyclerAdapter
import kotlinx.android.synthetic.main.fragment_pldog.*
import javax.inject.Inject

class PLDogFragment : BaseFragment() {
    companion object {
        fun newInstance(): Fragment = PLDogFragment()
    }

    @Inject
    lateinit var viewModel: PLDogViewModel
    @Inject
    lateinit var postsDiffUtil: PostsDiffUtil
    @Inject
    lateinit var postsDiffResult: DiffUtil.DiffResult
    @Inject
    lateinit var postsAdapter: PostsRecyclerAdapter
    @Inject
    lateinit var gridLayoutManager: StaggeredGridLayoutManager
    private var lastVisibleItem: Int = 0

    override fun layoutRes(): Int = R.layout.fragment_pldog

    override fun initViews() {
        srlDogPosts.setOnRefreshListener {
            viewModel.lastLoadedPage = 0
            viewModel.loadPosts()
        }
        fabDogUpward.setOnClickListener {
            rvDogPosts.smoothScrollToPosition(0)
        }
        rvDogPosts.setOnScrollChangeListener { _, _, _, _, _ ->
            var item = IntArray(2)
            item = gridLayoutManager.findLastVisibleItemPositions(item)
            lastVisibleItem = item[0]
            showUpwardFab()
            showNextListPortion()
        }
        rvDogPosts.layoutManager = gridLayoutManager
        rvDogPosts.adapter = postsAdapter
    }

    override fun initDisplayableList() {
        currentCategoryType = CategoryTypes.Dog
        viewModel.postListLiveData.observe(
            this,
            Observer { list ->
                if (postsAdapter.posts.isEmpty()) {
                    postsAdapter.posts = list
                    postsAdapter.notifyDataSetChanged()
                }
                else
                {
                    postsDiffUtil.oldList = postsAdapter.posts
                    postsDiffUtil.newList = list
                    postsDiffResult = DiffUtil.calculateDiff(postsDiffUtil)
                    postsAdapter.posts = list
                    postsDiffResult.dispatchUpdatesTo(postsAdapter)
                }
                srlDogPosts.isRefreshing = false
            }
        )
        viewModel.progressStateLiveData.observe(
            this,
            Observer {
                it.let {
                    if (it.progressVisible) {
                        pbDogPosts.visibility = View.VISIBLE
                        rvDogPosts.visibility = View.GONE
                    } else {
                        pbDogPosts.visibility = View.GONE
                        rvDogPosts.visibility = View.VISIBLE
                    }
                }
            }
        )
        viewModel.postListInit(currentCategoryType)
    }

    private fun showUpwardFab() {
        if (lastVisibleItem > Constants.THRESHOLD_RECYCLER_VISIBLE_ITEMS)
            fabDogUpward.visibility = View.VISIBLE
        else fabDogUpward.visibility = View.GONE
    }

    private fun showNextListPortion() {
        val totalItemCount = gridLayoutManager.itemCount
        if (!viewModel.loading && totalItemCount <= (lastVisibleItem + Constants.THRESHOLD_RECYCLER_LOADER)) {
            viewModel.loading = true
            activity?.logging(
                "Load new list of ${currentCategoryType.value}",
                Constants.LOADING_POSTS
            )
            viewModel.loadPosts()
        }
    }
}
