package com.plans.rush.mediator.presentation.feature.postsingle.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.plans.rush.mediator.di.scope.ViewModelKey
import com.plans.rush.mediator.domain.interactor.*
import com.plans.rush.mediator.domain.usecase.*
import com.plans.rush.mediator.domain.usecase.postinner.*
import com.plans.rush.mediator.presentation.base.types.PageTypes
import com.plans.rush.mediator.presentation.feature.postsingle.PostSingleActivity
import com.plans.rush.mediator.presentation.feature.postsingle.adapter.InfoRecyclerAdapter
import com.plans.rush.mediator.presentation.feature.postsingle.adapter.MediasRecyclerAdapter
import com.plans.rush.mediator.presentation.feature.postsingle.viewmodel.PostSingleViewModel
import com.plans.rush.mediator.presentation.viewmodel.mapper.CommentsAndTagsEntityVMMapper
import com.plans.rush.mediator.presentation.viewmodel.mapper.ImageEntityVMMapper
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap

@Module
class PostSingleModule {
    @Provides
    @IntoMap
    @ViewModelKey(PostSingleViewModel::class)
    fun provideViewModel(
        postLoaders: PostLoaders,
        postObservers: PostObservers,
        commentLoaders: CommentLoaders,
        imageLoaders: ImageLoaders,
        insertCoverImageUseCase: InsertCoverImageUseCase,
        imageEntityVMMapper: ImageEntityVMMapper,
        commentsAndTagsEntityVMMapper: CommentsAndTagsEntityVMMapper,
        mediaDownloadUseCase: MediaDownloadUseCase
    ): ViewModel =
        PostSingleViewModel(
            postLoaders, postObservers, commentLoaders,
            imageLoaders, insertCoverImageUseCase, imageEntityVMMapper,
            commentsAndTagsEntityVMMapper, mediaDownloadUseCase
        )

    @Provides
    fun provideMainViewModel(
        activity: PostSingleActivity,
        factory: ViewModelProvider.Factory
    ): PostSingleViewModel =
        ViewModelProviders.of(activity, factory).get(PostSingleViewModel::class.java)

    @Provides
    fun provideLayoutManagerInfo(activity: PostSingleActivity): LinearLayoutManager =
        LinearLayoutManager(activity, RecyclerView.VERTICAL, false)

    @Provides
    fun provideMediasAdapter(): MediasRecyclerAdapter =
        MediasRecyclerAdapter()

    @Provides
    fun provideInfoAdapter(activity: PostSingleActivity): InfoRecyclerAdapter =
        InfoRecyclerAdapter {
            val tags = arrayListOf(it)
            activity.fireActivity(PageTypes.SearchByTag, tags = tags)
        }
}