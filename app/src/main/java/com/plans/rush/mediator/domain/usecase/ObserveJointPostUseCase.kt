package com.plans.rush.mediator.domain.usecase

import com.plans.rush.mediator.domain.entity.JointPostEntity
import com.plans.rush.mediator.domain.irepository.IPostsRepository
import com.plans.rush.mediator.presentation.base.types.CategoryTypes
import io.reactivex.Flowable
import javax.inject.Inject

class ObserveJointPostUseCase @Inject constructor(
    private val postsRepository: IPostsRepository
) {
    operator fun invoke(postId: String, categoryType: CategoryTypes): Flowable<JointPostEntity> =
        postsRepository.observeJointPostDeepLink(postId, categoryType)
}