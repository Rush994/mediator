package com.plans.rush.mediator.presentation.feature.plcat.viewmodel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.google.gson.Gson
import com.plans.rush.mediator.data.network.api.response.ErrorResp
import com.plans.rush.mediator.domain.interactor.PostLoaders
import com.plans.rush.mediator.domain.interactor.PostObservers
import com.plans.rush.mediator.presentation.base.BaseViewModel
import com.plans.rush.mediator.presentation.base.tempobject.StateObject
import com.plans.rush.mediator.presentation.base.types.CategoryTypes
import com.plans.rush.mediator.presentation.viewmodel.PostViewModel
import com.plans.rush.mediator.presentation.viewmodel.mapper.PostOuterEntityVMMapper
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.processors.PublishProcessor
import io.reactivex.schedulers.Schedulers
import retrofit2.HttpException
import javax.inject.Inject

class PLCatViewModel @Inject constructor(
    private val postLoaders: PostLoaders,
    private val postObservers: PostObservers,
    private val postOuterEntityVMMapper: PostOuterEntityVMMapper
): BaseViewModel() {
    val postListLiveData = MutableLiveData<List<PostViewModel>>()
    val progressStateLiveData = MutableLiveData<StateObject>()

    private var loadPaginator = PublishProcessor.create<Int>()
    var lastLoadedPage = 0
    var loading = false

    fun postListInit(categoryTypes: CategoryTypes) {
        lastLoadedPage = 0
        initObserver(categoryTypes)
        initLoader(categoryTypes)
        android.os.Handler().postDelayed({
            loadPosts()
        }, 100)
    }

    fun loadPosts() {
        lastLoadedPage++
        loadPaginator.onNext(lastLoadedPage)
    }

    private fun initObserver(categoryTypes: CategoryTypes) =
        postObservers.posts(categoryTypes.value)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .map { postOuterEntityVMMapper.mapFrom(it) }
            .subscribe({
                if(it.size>0) {
                    postListLiveData.value = it
                    progressStateLiveData.value = StateObject(
                        progressVisible = false,
                        completed = true
                    )
                    loading = false
                }
            },
                {
                    if (it is HttpException) {
                        val resp = it.response()?.errorBody()
                    }
                    Log.d("Not get: ", it.toString())
                })
            .also { disposables.add(it) }

    private fun initLoader(categoryTypes: CategoryTypes) =
        loadPaginator
            .concatMapCompletable {
                postLoaders.topPosts(page = it, postType = categoryTypes.value)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
            }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                progressStateLiveData.value = StateObject(
                    progressVisible = true,
                    completed = false
                )
            }
            .subscribe({
            },
                {
                    if (it is HttpException) {
                        val gson = Gson()
                        val resp = it.response()?.errorBody()
                        val errorObject = gson.fromJson(resp.toString(), ErrorResp::class.java)
                        Log.d("Not get: ", errorObject.error)
                    }
                    Log.d("Not get: ", it.toString())
                }).also { disposables.add(it) }

}