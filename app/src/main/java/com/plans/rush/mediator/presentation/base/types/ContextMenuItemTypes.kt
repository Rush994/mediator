package com.plans.rush.mediator.presentation.base.types

enum class ContextMenuItemTypes(val value: Int) {
    Download(1),
    Share(2)
}