package com.plans.rush.mediator.presentation.base.types

enum class CategoryTypes(val value: Int){
    Default(0),
    ByTag(1),
    Ordinal(2),
    Cat(3),
    Dog(4),
    DeepLink(5)
}