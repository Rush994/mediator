package com.plans.rush.mediator.presentation.feature.postsingle.listener

interface OnTagClickListener {
    fun searchByTagFiring(tag: String)
}