package com.plans.rush.mediator.presentation.viewmodel

import android.os.Parcelable
import com.plans.rush.mediator.presentation.base.types.CategoryTypes
import kotlinx.android.parcel.Parcelize

@Parcelize
data class PostViewModel(
    var id: String = "",
    var mediaId: String = "",
    val title: String = "",
    val datetime: Long = 0,
    val cover: String? = null,
    val owner: String = "",
    val views: Int = 0,
    val link: String = "",
    val thumbUps: Int = 0,
    val thumbDowns: Int = 0,
    val commentCount: Int = 0,
    val isAlbum: Boolean = false,
    var mediaCount: Int? = null,
    val categoryType: CategoryTypes = CategoryTypes.ByTag
): Parcelable