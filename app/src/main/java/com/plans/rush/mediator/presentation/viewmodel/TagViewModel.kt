package com.plans.rush.mediator.presentation.viewmodel

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class TagViewModel(
    val name: String,
    val displayName: String,
    val followers: Int,
    val totalItems: Int,
    var isChecked: Boolean = false
): Parcelable