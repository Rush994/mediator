package com.plans.rush.mediator.presentation.base.types

enum class CellTypes(val value: Int) {
    PostTitle(1),
    PostInfo(2),
    PostTag(3),
    PostComment(4)
}