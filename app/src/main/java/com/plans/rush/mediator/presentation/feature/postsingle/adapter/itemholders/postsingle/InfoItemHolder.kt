package com.plans.rush.mediator.presentation.feature.postsingle.adapter.itemholders.postsingle

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.plans.rush.mediator.presentation.viewmodel.PostViewModel
import kotlinx.android.synthetic.main.post_single_info_item.view.*

class InfoItemHolder(private val view: View): RecyclerView.ViewHolder(view) {
    fun bind(item: PostViewModel){
        with(view){
            tvMediaThumsUp.text = item.thumbUps.toString()
            tvMediaThumsDown.text = item.thumbDowns.toString()
        }
    }
}