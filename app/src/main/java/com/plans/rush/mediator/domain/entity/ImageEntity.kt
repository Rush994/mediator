package com.plans.rush.mediator.domain.entity


data class ImageEntity(
    val postId: String,
    val mediaId: String,
    val title: String?,
    val description: String?,
    val datetime: Long,
    val mp4: String?,
    val link: String
)