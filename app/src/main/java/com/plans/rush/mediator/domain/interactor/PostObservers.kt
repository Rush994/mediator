package com.plans.rush.mediator.domain.interactor

import com.plans.rush.mediator.domain.usecase.postinner.*
import com.plans.rush.mediator.domain.usecase.ObserveJointPostUseCase
import com.plans.rush.mediator.domain.usecase.postouter.ObservePostListTopUseCase
import com.plans.rush.mediator.presentation.base.types.CategoryTypes
import javax.inject.Inject

class PostObservers @Inject constructor(
    private val observeImagesUseCase: ObserveImagesUseCase,
    private val observePostListTopUseCase: ObservePostListTopUseCase,
    private val observeJointPostUseCase: ObserveJointPostUseCase
) {
    fun images(postId: String) =
        observeImagesUseCase(postId)
    fun posts(postType: Int) =
        observePostListTopUseCase(postType)
    fun  jointPost(postId: String, categoryType: CategoryTypes) =
        observeJointPostUseCase(postId, categoryType)
}