package com.plans.rush.mediator.presentation.feature.postsingle.listener

interface DetachViewListener {
    fun releasePlayer()
    fun pauseLastVideo()
    fun resumeLastVideo()
}