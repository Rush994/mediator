package com.plans.rush.mediator.domain.irepository

import android.content.Context
import com.plans.rush.mediator.data.network.response.PostResp
import com.plans.rush.mediator.domain.entity.*
import com.plans.rush.mediator.presentation.base.types.CategoryTypes
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single

interface IPostsRepository {

    fun loadPost(postId: String, categoryType: Int): Completable
    fun findPost(postId: String): Flowable<PostEntity>

    fun observePosts(postType: Int): Flowable<List<PostEntity>>

    fun observeTags(postId: String): Flowable<List<TagEntity>>

    fun loadPostsDependsOnType(name: String, page: Int, postType: Int): Single<List<PostResp>>

    fun loadPosts(name:String, page: Int, postType: Int): Completable

    fun observeImages(postId: String): Flowable<List<ImageEntity>>

    fun observeJointPost(postId: String): Flowable<JointPostEntity>
    fun observeJointPostDeepLink(postId: String, categoryType: CategoryTypes): Flowable<JointPostEntity>

    fun insertImage(post: PostEntity): Completable

    fun loadImages(postId: String, postType: Int): Completable

    fun loadComments(postId: String): Completable

    fun deletePostsByTag(): Completable

    fun loadTags(): Single<List<TagEntity>>

    fun mediaDownload(folderPath:String, filePath: String, mediaUri: String, context: Context): Completable
}