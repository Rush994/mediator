package com.plans.rush.mediator.data.db.model

import androidx.room.*
import com.plans.rush.mediator.data.db.AppDatabase

@Entity(tableName = AppDatabase.TABLE_POSTS_TOP)
data class PostModel(
    @PrimaryKey
    @ColumnInfo(name = "Id", index = true)
    val id: String,
    @ColumnInfo(name = "ImageId")
    var mediaId: String,
    @ColumnInfo(name = "PostTitle")
    val title: String,
    @ColumnInfo(name = "DateTime")
    val datetime: Long,
    @ColumnInfo(name = "Cover")
    val cover: String?,
    @ColumnInfo(name = "Owner")
    val owner: String,
    @ColumnInfo(name = "Views")
    val views: Int,
    @ColumnInfo(name = "Link")
    val link: String,
    @ColumnInfo(name = "ThumbUps")
    val thumbUps: Int,
    @ColumnInfo(name = "ThumbDowns")
    val thumbDowns: Int,
    @ColumnInfo(name = "CommentCount")
    val commentCount: Int,
    @ColumnInfo(name = "IsAlbum")
    val isAlbum: Boolean,
    @ColumnInfo(name = "MediaCount")
    val mediaCount: Int?,
    @ColumnInfo(name = "PostType")
    val postType: Int
)