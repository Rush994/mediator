package com.plans.rush.mediator.data.db.source

import com.plans.rush.mediator.data.db.model.*
import io.reactivex.Completable
import io.reactivex.Flowable

interface IPostsDBSource {

    fun observePost(postId: String): Flowable<PostModel>

    fun observePosts(postType: Int): Flowable<List<PostModel>>

    fun observeTags(postId: String): Flowable<List<TagModel>>

    fun observeComments(postId: String): Flowable<List<CommentModel>>

    fun insertPosts(jointPosts: List<JointPostModel>): Completable

    fun observeImages(postId: String): Flowable<List<ImageModel>>

    fun insertImage(image: ImageModel): Completable

    fun insertImages(jointPost: JointPostModel): Completable

    fun insertComments(commentList: List<CommentModel>): Completable

    fun deletePosts(postType: Int): Completable
    fun deletePostsByTag(): Completable
}