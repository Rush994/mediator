package com.plans.rush.mediator.presentation.feature.postsingle

import android.Manifest
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.view.WindowManager
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.plans.rush.mediator.R
import com.plans.rush.mediator.presentation.base.BaseActivity
import com.plans.rush.mediator.presentation.base.Constants
import com.plans.rush.mediator.presentation.base.showNotificationOnDownloadFinish
import com.plans.rush.mediator.presentation.base.types.CategoryTypes
import com.plans.rush.mediator.presentation.base.types.ContextMenuItemTypes
import com.plans.rush.mediator.presentation.feature.postsingle.adapter.InfoRecyclerAdapter
import com.plans.rush.mediator.presentation.feature.postsingle.adapter.MediasRecyclerAdapter
import com.plans.rush.mediator.presentation.feature.postsingle.adapter.itemholders.media.VideoItemHolder
import com.plans.rush.mediator.presentation.feature.postsingle.listener.DetachViewListener
import com.plans.rush.mediator.presentation.feature.postsingle.viewmodel.PostSingleViewModel
import com.plans.rush.mediator.presentation.viewmodel.ImageViewModel
import com.plans.rush.mediator.presentation.viewmodel.PostViewModel
import kotlinx.android.synthetic.main.activity_post_single.*
import pub.devrel.easypermissions.EasyPermissions
import javax.inject.Inject


class PostSingleActivity : BaseActivity(),
    EasyPermissions.PermissionCallbacks,
    DetachViewListener {

    @Inject
    lateinit var viewModel: PostSingleViewModel
    @Inject
    lateinit var layoutManagerInfo: LinearLayoutManager
    @Inject
    lateinit var layoutManagerMedias: LinearLayoutManager
    @Inject
    lateinit var infoAdapter: InfoRecyclerAdapter
    @Inject
    lateinit var mediasAdapter: MediasRecyclerAdapter

    private lateinit var downloadURI: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_post_single)

        // Если запускаем не с дип линка
        if (intent.data == null) {
            viewModel.post =
                intent.getParcelableExtra(Constants.POST_DATA) as PostViewModel
            viewModel.mediaUri = "https://imgur.com/gallery/${viewModel.post.id}"
        } else {
            viewModel.mediaUri = "${intent.data}"
            viewModel.post = PostViewModel(
                id = viewModel.mediaUri.reversed().substringBefore('/').reversed(),
                categoryType = CategoryTypes.DeepLink,
                isAlbum = true
            )
        }
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)

        rvPostMedias.layoutManager = layoutManagerMedias
        rvPostMedias.adapter = mediasAdapter
        viewModel.observedImagesLiveData.observe(
            this,
            Observer { list ->
                mediasAdapter.setMediaList(list)
                viewModel.appName = getString(R.string.app_name)
            }
        )

        rvPostInfos.adapter = infoAdapter
        rvPostInfos.layoutManager = layoutManagerInfo
        viewModel.observedCommentsLiveData.observe(
            this,
            Observer { item ->
                if (viewModel.isDeepLink) {
                    infoAdapter.setPost(item.post)
                    viewModel.isDeepLink = false
                } else {
                    infoAdapter.setPost(viewModel.post)
                }
                infoAdapter.setTagList(item.tags)
                infoAdapter.setCommentList(item.comments)
            }
        )

        viewModel.statesLiveData.observe(
            this,
            Observer { state ->
                state?.let {
                    if (it.progressVisible) {
                    } else {
                        showNotificationOnDownloadFinish(viewModel.filePath, viewModel.isMp4)
                    }
                }

            })

        viewModel.selectObservedSource()
    }

    override fun onContextItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            ContextMenuItemTypes.Download.value ->
                downloadItem(mediasAdapter.medias.get(item.groupId))
            ContextMenuItemTypes.Share.value ->
                shareItem(mediasAdapter.medias.get(item.groupId))
        }
        return true
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        EasyPermissions.onRequestPermissionsResult(
            requestCode,
            permissions,
            grantResults,
            this
        )
    }

    override fun onPermissionsGranted(requestCode: Int, perms: List<String>) {
        viewModel.mediaDownloading(downloadURI, this)
    }

    override fun onPermissionsDenied(requestCode: Int, perms: List<String>) {
        Log.d(Constants.PERMISSION_TAG, "Permission has been denied")
    }

    override fun onResume() {
        super.onResume()
        resumeLastVideo()
    }
    override fun resumeLastVideo() {
        val holder = rvPostMedias.findViewHolderForLayoutPosition(
            layoutManagerMedias.findLastVisibleItemPosition()
        )
        if (holder is VideoItemHolder)
            if (holder.pausedOnCollapse) {
                holder.resumeVideo()
                holder.pausedOnCollapse = false
            }
    }

    override fun onPause() {
        super.onPause()
        pauseLastVideo()
    }
    override fun pauseLastVideo() {
        val holder = rvPostMedias.findViewHolderForLayoutPosition(
            layoutManagerMedias.findLastVisibleItemPosition()
        )
        if (holder is VideoItemHolder)
            holder.pauseVideo(true)
    }

    override fun onDestroy() {
        super.onDestroy()
        releasePlayer()
    }

    override fun releasePlayer() {
        for (i in mediasAdapter.medias.indices) {
            val holder = rvPostMedias.findViewHolderForLayoutPosition(i)
            if (holder is VideoItemHolder)
                holder.releasePlayer()
        }
    }

    private fun shareItem(item: ImageViewModel) {
        val intent = Intent(Intent.ACTION_SEND)
        intent.type = "text/plain"
        intent.putExtra(Intent.EXTRA_SUBJECT, "Shared by \"${viewModel.appName}\": ${item.title}")
        intent.putExtra(Intent.EXTRA_TEXT, item.link)
        startActivity(Intent.createChooser(intent, "Share post URL"))
    }

    private fun downloadItem(item: ImageViewModel) {
        downloadURI = item.link
        val canSave = viewModel.isSDCardPresent()
        if (canSave) {
            if (EasyPermissions.hasPermissions(
                    this@PostSingleActivity,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                )
            ) {
                viewModel.mediaDownloading(downloadURI, this)
            } else {
                EasyPermissions.requestPermissions(
                    this@PostSingleActivity,
                    getString(R.string.writeFile),
                    Constants.WRITE_REQUEST_CODE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                )
            }
        } else {
            Toast.makeText(
                applicationContext,
                "SD Card not found", Toast.LENGTH_LONG
            ).show()
        }
    }

    fun clickOnSharePost(v: View) {
        val intent = Intent(Intent.ACTION_SEND)
        intent.type = "text/plain"
        intent.putExtra(
            Intent.EXTRA_SUBJECT,
            "Shared by \"${viewModel.appName}\": ${viewModel.post.title}"
        )
        intent.putExtra(Intent.EXTRA_TEXT, "https://imgur.com/gallery/${viewModel.post.id}")
        startActivity(Intent.createChooser(intent, "Share post URL"))
    }
}
