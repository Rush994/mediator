package com.plans.rush.mediator.domain.common

interface SingleMapper<in E, T> {
    fun mapFrom(item: E): T
}