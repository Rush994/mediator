package com.plans.rush.mediator.data.network.response

import com.google.gson.annotations.SerializedName

data class ImageResp(
    @SerializedName("id")
    val id: String,
    @SerializedName("title")
    val title: String?,
    @SerializedName("description")
    val description: String?,
    @SerializedName("datetime")
    val datetime: Long,
    @SerializedName("mp4")
    val mp4: String?,
    @SerializedName("link")
    val link: String
)