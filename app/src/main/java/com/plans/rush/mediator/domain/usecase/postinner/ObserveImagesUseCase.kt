package com.plans.rush.mediator.domain.usecase.postinner

import com.plans.rush.mediator.domain.entity.ImageEntity
import com.plans.rush.mediator.domain.irepository.IPostsRepository
import io.reactivex.Flowable
import io.reactivex.Observable
import javax.inject.Inject

class ObserveImagesUseCase @Inject constructor(
    private val postsRepository: IPostsRepository
){
    operator fun invoke(postId: String): Flowable<List<ImageEntity>> =
        postsRepository.observeImages(postId)
}