package com.plans.rush.mediator.data.network.response

import com.google.gson.annotations.SerializedName

data class CommentResp(
    @SerializedName(value = "id")
    val id: Long,
    @SerializedName(value = "image_id")
    val imageId: String,
    @SerializedName(value = "comment")
    val comment: String,
    @SerializedName(value = "author")
    val author: String,
    @SerializedName(value = "ups")
    val ups: Long,
    @SerializedName(value = "downs")
    val downs: Long,
    @SerializedName(value = "platform")
    val platform: String
)