package com.plans.rush.mediator.data.db.dao

import androidx.room.Dao
import com.plans.rush.mediator.data.db.AppDatabase
import com.plans.rush.mediator.data.db.model.TagModel

@Dao
abstract class TagsDao: BaseDao<TagModel>(AppDatabase.TABLE_POST_TAGS) {

}