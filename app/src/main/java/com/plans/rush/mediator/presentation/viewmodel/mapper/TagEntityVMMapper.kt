package com.plans.rush.mediator.presentation.viewmodel.mapper

import com.plans.rush.mediator.domain.common.ListMapper
import com.plans.rush.mediator.domain.entity.TagEntity
import com.plans.rush.mediator.presentation.viewmodel.TagViewModel
import javax.inject.Inject

class TagEntityVMMapper @Inject constructor(): ListMapper<TagEntity, TagViewModel> {
    override fun mapFrom(item: TagEntity): TagViewModel =
        TagViewModel(
            name = item.name,
            displayName = item.displayName,
            followers = item.followers,
            totalItems = item.totalItems
        )

    override fun mapFrom(list: List<TagEntity>): List<TagViewModel> =
        list.map { mapFrom(it) }

}