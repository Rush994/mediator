package com.plans.rush.mediator.data.local.source

import android.content.Context
import io.reactivex.Completable

interface IPostsLocalSource {
    fun mediaDownload(folderPath:String, filePath: String, mediaUri: String, context: Context): Completable
}