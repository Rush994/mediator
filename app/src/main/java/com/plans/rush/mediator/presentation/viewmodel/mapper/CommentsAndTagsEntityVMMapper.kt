package com.plans.rush.mediator.presentation.viewmodel.mapper

import com.plans.rush.mediator.domain.common.ListMapper
import com.plans.rush.mediator.domain.entity.JointPostEntity
import com.plans.rush.mediator.presentation.viewmodel.JointPostViewModel
import javax.inject.Inject

class CommentsAndTagsEntityVMMapper @Inject constructor(
    private val commentEntityVMMapper: CommentEntityVMMapper,
    private val tagEntityVMMapper: TagEntityVMMapper,
    private val postOuterEntityVMMapper: PostOuterEntityVMMapper
): ListMapper<JointPostEntity, JointPostViewModel> {
    override fun mapFrom(item: JointPostEntity): JointPostViewModel =
        JointPostViewModel(
            comments = commentEntityVMMapper.mapFrom(item.comments),
            tags = tagEntityVMMapper.mapFrom(item.tags),
            post = postOuterEntityVMMapper.mapFrom(item.post)
        )

    override fun mapFrom(list: List<JointPostEntity>): List<JointPostViewModel> =
        list.map { mapFrom(it) }

}