package com.plans.rush.mediator.di.module

import com.plans.rush.mediator.presentation.feature.postlist.PostListActivity
import com.plans.rush.mediator.presentation.feature.postlist.di.PostListModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class OuterFragmentBuildersModule {
    @ContributesAndroidInjector(modules = arrayOf(
        PostListModule::class))
    abstract fun contributeBestPostListActivity(): PostListActivity
}