package com.plans.rush.mediator.data.db.model

data class JointPostModel(
    val post: PostModel,
    val images: List<ImageModel>,
    val tags: List<TagModel>
)