package com.plans.rush.mediator.presentation.base.tempobject

data class FinalizeObject(val textWillAppear: String)