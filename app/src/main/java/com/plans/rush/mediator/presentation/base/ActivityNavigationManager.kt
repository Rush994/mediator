package com.plans.rush.mediator.presentation.base

import android.content.Context
import android.content.Intent
import com.plans.rush.mediator.presentation.base.types.PageTypes
import com.plans.rush.mediator.presentation.feature.postlist.PostListActivity
import com.plans.rush.mediator.presentation.feature.postlistbytag.PostListByTagActivity
import com.plans.rush.mediator.presentation.feature.postsingle.PostSingleActivity
import com.plans.rush.mediator.presentation.feature.searchbytag.SearchByTagActivity
import com.plans.rush.mediator.presentation.viewmodel.PostViewModel
import javax.inject.Inject

class ActivityNavigationManager @Inject constructor() {

    var currentPage = PageTypes.None

    private var pageContext: Context? = null

    fun Navigate(page: PageTypes, context: Context, postData: PostViewModel, tags: ArrayList<String>) {
        pageContext = context
        navigate(page, postData, tags)
    }

    private fun navigate(page: PageTypes, postData: PostViewModel, tags: ArrayList<String>) {
        currentPage = page
        when (page) {
            PageTypes.PostList -> pageContext?.startActivity(Intent(pageContext, PostListActivity::class.java))
            PageTypes.PostSingle -> pageContext?.startActivity(Intent(pageContext, PostSingleActivity::class.java).putExtra(Constants.POST_DATA, postData))
            PageTypes.PreSearch -> pageContext?.startActivity(Intent(pageContext, SearchByTagActivity::class.java))
            PageTypes.SearchByTag -> pageContext?.startActivity(Intent(pageContext, PostListByTagActivity::class.java).putExtra(Constants.POST_DATA, tags))
            PageTypes.None -> TODO()
        }
    }
}