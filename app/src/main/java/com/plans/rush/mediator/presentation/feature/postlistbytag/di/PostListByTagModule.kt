package com.plans.rush.mediator.presentation.feature.postlistbytag.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.plans.rush.mediator.di.scope.ViewModelKey
import com.plans.rush.mediator.domain.interactor.PostLoaders
import com.plans.rush.mediator.domain.interactor.PostObservers
import com.plans.rush.mediator.domain.usecase.postouter.DeletePostsByTagUseCase
import com.plans.rush.mediator.presentation.feature.postlist.adapter.PostsRecyclerAdapter
import com.plans.rush.mediator.presentation.feature.postlistbytag.PostListByTagActivity
import com.plans.rush.mediator.presentation.feature.postlistbytag.viewmodel.PostListByTagViewModel
import com.plans.rush.mediator.presentation.viewmodel.mapper.PostOuterEntityVMMapper
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap

@Module
class PostListByTagModule {
    @Provides
    @IntoMap
    @ViewModelKey(PostListByTagViewModel::class)
    fun provideViewModel(postLoaders: PostLoaders,
                         postObservers: PostObservers,
                         deletePostsByTagUseCase: DeletePostsByTagUseCase,
                         postOuterEntityVMMapper: PostOuterEntityVMMapper
    ): ViewModel =
        PostListByTagViewModel(
            postLoaders, postObservers,
            deletePostsByTagUseCase, postOuterEntityVMMapper)

    @Provides
    fun providePostListViewModel(activity: PostListByTagActivity, factory: ViewModelProvider.Factory): PostListByTagViewModel =
        ViewModelProviders.of(activity, factory).get(PostListByTagViewModel::class.java)

    @Provides
    fun provideStaggeredManager(): StaggeredGridLayoutManager =
        StaggeredGridLayoutManager(2, RecyclerView.VERTICAL)

    @Provides
    fun providePostsAdapter(activity: PostListByTagActivity): PostsRecyclerAdapter =
        PostsRecyclerAdapter(activity)
}