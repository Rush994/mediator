package com.plans.rush.mediator.presentation.feature.postlistbytag

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.plans.rush.mediator.R
import com.plans.rush.mediator.presentation.base.BaseActivity
import com.plans.rush.mediator.presentation.base.Constants
import com.plans.rush.mediator.presentation.base.types.PageTypes
import com.plans.rush.mediator.presentation.feature.postlist.adapter.PostsRecyclerAdapter
import com.plans.rush.mediator.presentation.feature.postlist.listener.OnPostClickListener
import com.plans.rush.mediator.presentation.feature.postlistbytag.viewmodel.PostListByTagViewModel
import com.plans.rush.mediator.presentation.viewmodel.PostViewModel
import kotlinx.android.synthetic.main.activity_post_list_by_tag.*
import javax.inject.Inject

class PostListByTagActivity : BaseActivity(), OnPostClickListener {

    @Inject
    lateinit var viewModel: PostListByTagViewModel
    @Inject
    lateinit var gridLayoutManager: StaggeredGridLayoutManager
    @Inject
    lateinit var postsAdapter: PostsRecyclerAdapter

    private var lastVisibleItem: Int = 0
    private lateinit var scrollUpwardListener: RecyclerView.OnScrollListener
    private lateinit var scrollLoaderListener: RecyclerView.OnScrollListener

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_post_list_by_tag)

        val tagArray: ArrayList<String> = intent.getStringArrayListExtra(Constants.POST_DATA)!!
        initViews(tagArray)
        initDisplayableList(tagArray)
    }

    override fun onItemClick(item: PostViewModel) {
        fireActivity(PageTypes.PostSingle, item)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        viewModel.deletePostsByTag()
    }

    private fun initViews(tagArray: ArrayList<String>){
        supportActionBar?.title = "Searched for \"${tagArray[0]}\""

        rvPostsByTag.layoutManager = gridLayoutManager
        rvPostsByTag.adapter = postsAdapter

        srlPostsByTag.setOnRefreshListener {
            viewModel.lastLoadedPage = 0
            viewModel.loadPosts(tagArray)
        }
        fabUpwardByTag.setOnClickListener {
            rvPostsByTag.smoothScrollToPosition(0)
        }
        rvPostsByTag.setOnScrollChangeListener { v, scrollX, scrollY, oldScrollX, oldScrollY ->
            var item = IntArray(2)
            item = gridLayoutManager.findLastVisibleItemPositions(item)
            lastVisibleItem = item[0]
            showUpwardFab()
            showNextListPortion()
        }
    }

    private fun initDisplayableList(tagArray: ArrayList<String>) {
        viewModel.progressStateLiveData.observe(
            this,
            Observer {
                it.let {
                    if (it.progressVisible) {
                        pbPostsByTag.visibility = View.VISIBLE
                        rvPostsByTag.visibility = View.GONE
                    } else {
                        pbPostsByTag.visibility = View.GONE
                        rvPostsByTag.visibility = View.VISIBLE
                    }
                }
            }
        )
        viewModel.postListLiveData.observe(
            this,
            Observer { list ->
                postsAdapter.posts = list
                postsAdapter.notifyDataSetChanged()
                srlPostsByTag.isRefreshing = false
            }
        )
        viewModel.postListInit(tagArray)
    }

    private fun showUpwardFab() {
        if (lastVisibleItem > 10)
            fabUpwardByTag.visibility = View.VISIBLE
        else
            fabUpwardByTag.visibility = View.GONE
    }

    private fun showNextListPortion() {
        val totalItemCount = gridLayoutManager.itemCount
        if (!viewModel.loading && totalItemCount <= (lastVisibleItem + 3))
            viewModel.loading = true
    }
}
