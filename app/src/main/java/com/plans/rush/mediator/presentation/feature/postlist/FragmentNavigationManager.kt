package com.plans.rush.mediator.presentation.feature.postlist

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import javax.inject.Inject

class FragmentNavigationManager @Inject constructor(
    private val fm: FragmentManager,
    private val container: Int
) {
    private val mapFr = hashMapOf<String, Fragment>()
    private var currentFragmentName: String? = null

    fun navigateTo(fragment: Fragment) {

        val newFragmentName = fragment::class.java.name

        currentFragmentName?.let { currentName ->

            if (currentName != newFragmentName) {
                val currentFragment = mapFr[currentName]!!

                mapFr[newFragmentName]?.let { newFragment ->

                    currentFragmentName = newFragmentName
                    fm.beginTransaction()
                        .hide(currentFragment)
                        .show(newFragment)
                        .commit()
                } ?: add(fragment, newFragmentName, currentFragment)
            }
        } ?: add(fragment, newFragmentName)

    }

    private fun add(fragment: Fragment,
                    newFragmentName: String,
                    currentFragment: Fragment? = null) {
        currentFragmentName = newFragmentName
        mapFr[newFragmentName] = fragment
        fm.beginTransaction()
            .apply {
                currentFragment?.let { hide(it) }
                add(container, fragment, currentFragmentName)
                commit()
            }

    }
}