package com.plans.rush.mediator.data.db.model.mapper

import com.plans.rush.mediator.data.db.model.CommentModel
import com.plans.rush.mediator.domain.entity.CommentEntity
import javax.inject.Inject

class CommentModelEntityMapper @Inject constructor() {
    //region Ordinal
    fun mapFrom(item: CommentModel): CommentEntity =
        CommentEntity(
            id = item.id,
            imageId = item.imageId,
            comment = item.comment,
            author = item.author,
            ups = item.ups,
            downs = item.downs,
            platform = item.platform
        )
    fun mapFrom(list: List<CommentModel>): List<CommentEntity> =
        list.map { mapFrom(it) }
    //endregion
}