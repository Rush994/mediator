package com.plans.rush.mediator.domain.usecase.postouter

import com.plans.rush.mediator.domain.irepository.IPostsRepository
import io.reactivex.Completable
import javax.inject.Inject

class LoadPostListTopUseCase @Inject constructor(
    private val postsRepository: IPostsRepository
) {
    operator fun invoke(name: String, page: Int, postType: Int): Completable =
        postsRepository.loadPosts(name, page, postType)
}