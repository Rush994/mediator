package com.plans.rush.mediator.data.network.response.mapper

import com.plans.rush.mediator.data.db.model.CommentModel
import com.plans.rush.mediator.data.network.response.CommentResp
import javax.inject.Inject

class CommentRespModelMapper @Inject constructor() {
    // region Ordinal
    private fun mapTo(item: CommentResp, postId: String): CommentModel =
        CommentModel(
            id = item.id,
            imageId = item.imageId,
            comment = item.comment,
            author = item.author,
            ups = item.ups,
            downs = item.downs,
            platform = item.platform,
            postId = postId
        )
    fun mapTo(list: List<CommentResp>, postId: String): List<CommentModel> =
        list.map { mapTo(it, postId) }
    //endregion
}