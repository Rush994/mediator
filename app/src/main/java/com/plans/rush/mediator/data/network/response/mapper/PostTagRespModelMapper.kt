package com.plans.rush.mediator.data.network.response.mapper

import com.plans.rush.mediator.data.db.model.TagModel
import com.plans.rush.mediator.data.network.response.TagResp
import com.plans.rush.mediator.domain.common.ListMapper
import javax.inject.Inject

class PostTagRespModelMapper @Inject constructor(): ListMapper<TagResp, TagModel> {
    override fun mapFrom(item: TagResp): TagModel =
        TagModel(
            name = item.name,
            displayName = item.displayName,
            followers = item.followers,
            totalItems = item.totalItems
        )

    override fun mapFrom(list: List<TagResp>): List<TagModel> =
        list.map { mapFrom(it) }

}