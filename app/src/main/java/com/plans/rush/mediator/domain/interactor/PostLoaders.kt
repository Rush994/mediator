package com.plans.rush.mediator.domain.interactor

import com.plans.rush.mediator.domain.usecase.postinner.LoadPostSingleDeepLinkUseCase
import com.plans.rush.mediator.domain.usecase.postouter.LoadPostListTopUseCase
import com.plans.rush.mediator.presentation.base.types.CategoryTypes
import javax.inject.Inject

class PostLoaders @Inject constructor(
    private val loadPostListTopUseCase: LoadPostListTopUseCase,
    private val loadPostSingleDeepLinkUseCase: LoadPostSingleDeepLinkUseCase
) {
    fun topPosts(name: String = "", page: Int, postType: Int) =
        loadPostListTopUseCase(name, page, postType)

    fun deepPost(postId: String) =
        loadPostSingleDeepLinkUseCase(postId, CategoryTypes.DeepLink.value)
}