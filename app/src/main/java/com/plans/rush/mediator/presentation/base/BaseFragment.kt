package com.plans.rush.mediator.presentation.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import com.plans.rush.mediator.presentation.base.types.CategoryTypes
import dagger.android.support.DaggerFragment
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

abstract class BaseFragment: DaggerFragment() {

    protected val compositeDisposable = CompositeDisposable()

    fun Disposable.disposeOnDestroy() = compositeDisposable.add(this)

    lateinit var currentCategoryType: CategoryTypes

    override fun onDestroyView() {
        super.onDestroyView()
        compositeDisposable.clear()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(layoutRes(), container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        initViews()
        initDisplayableList()
    }

    protected abstract fun initViews()

    protected abstract fun initDisplayableList()

    @LayoutRes
    protected abstract fun layoutRes(): Int
}