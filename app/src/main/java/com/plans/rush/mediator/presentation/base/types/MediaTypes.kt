package com.plans.rush.mediator.presentation.base.types

enum class MediaTypes(val value: Int) {
    Image(1),
    Video(2)
}