package com.plans.rush.mediator.presentation.feature.searchbytag.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import com.plans.rush.mediator.di.scope.ViewModelKey
import com.plans.rush.mediator.domain.usecase.LoadTagListUseCase
import com.plans.rush.mediator.presentation.feature.searchbytag.SearchByTagActivity
import com.plans.rush.mediator.presentation.feature.searchbytag.adapter.TagsRecyclerAdapter
import com.plans.rush.mediator.presentation.feature.searchbytag.viewmodel.SearchViewModel
import com.plans.rush.mediator.presentation.viewmodel.mapper.TagEntityVMMapper
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap

@Module
class SearchByTagModule {
    @Provides
    @IntoMap
    @ViewModelKey(SearchViewModel::class)
    fun provideViewModel(
        loadTagListUseCase: LoadTagListUseCase,
        tagEntityVMMapper: TagEntityVMMapper
    ): ViewModel =
        SearchViewModel(
            loadTagListUseCase,
            tagEntityVMMapper
        )

    @Provides
    fun provideSearchViewModel(
        activity: SearchByTagActivity,
        factory: ViewModelProvider.Factory
    ): SearchViewModel =
        ViewModelProviders.of(activity, factory).get(SearchViewModel::class.java)

    @Provides
    fun provideGridLayoutManager(activity: SearchByTagActivity): GridLayoutManager =
        GridLayoutManager(activity, 3)

    @Provides
    fun provideTagsAdapter(activity: SearchByTagActivity): TagsRecyclerAdapter =
        TagsRecyclerAdapter(activity)
}