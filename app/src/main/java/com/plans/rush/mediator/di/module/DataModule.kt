package com.plans.rush.mediator.di.module

import com.plans.rush.mediator.data.db.source.IPostsDBSource
import com.plans.rush.mediator.data.db.source.PostsDBSource
import com.plans.rush.mediator.data.local.source.IPostsLocalSource
import com.plans.rush.mediator.data.local.source.PostsLocalSource
import com.plans.rush.mediator.data.network.source.IPostsNetSource
import com.plans.rush.mediator.data.network.source.PostsNetSource
import com.plans.rush.mediator.data.repository.PostsRepository
import com.plans.rush.mediator.di.scope.PerApplication
import com.plans.rush.mediator.domain.irepository.IPostsRepository
import dagger.Binds
import dagger.Module

@Module
abstract class DataModule {

    @PerApplication
    @Binds
    abstract fun bindPostsNetSource(source: PostsNetSource): IPostsNetSource

    @PerApplication
    @Binds
    abstract fun bindPostsDBSource(source: PostsDBSource): IPostsDBSource

    @PerApplication
    @Binds
    abstract fun bindPostsLocalSource(source: PostsLocalSource): IPostsLocalSource

    @PerApplication
    @Binds
    abstract fun bindPostsRepository(source: PostsRepository): IPostsRepository
}