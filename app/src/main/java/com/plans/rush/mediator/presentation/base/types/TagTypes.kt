package com.plans.rush.mediator.presentation.base.types

enum class TagTypes(val value: String){
    Cat("cats"),
    Dog("dogs_are_the_best_people")
}