package com.plans.rush.mediator.domain.usecase.postinner

import com.plans.rush.mediator.domain.entity.PostEntity
import com.plans.rush.mediator.domain.irepository.IPostsRepository
import javax.inject.Inject

class InsertCoverImageUseCase @Inject constructor(
    private val postsRepository: IPostsRepository
) {
    operator fun invoke(post: PostEntity) =
        postsRepository.insertImage(post)

}