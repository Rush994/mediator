package com.plans.rush.mediator.data.network.source

import com.plans.rush.mediator.data.network.api.response.*
import com.plans.rush.mediator.data.network.response.PostResp
import io.reactivex.Single

interface IPostsNetSource {
    fun getPostListTop(page: Int): Single<PostListResp>
    fun getPostListCat(page: Int): Single<PostListByTagResp>
    fun getPostListDog(page: Int): Single<PostListByTagResp>
    fun getPostListByTag(name: String, page: Int): Single<PostListByTagResp>
    
    fun getPostInner(postId: String): Single<PostSingleResp>

    fun getCommentList(postId: String): Single<CommentListResp>

    fun getTagList(): Single<TagListDataResp>
}