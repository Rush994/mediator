package com.plans.rush.mediator.presentation.feature.searchbytag.listener

import com.plans.rush.mediator.presentation.viewmodel.TagViewModel

interface OnTagClickListener {
    fun onItemClick(item: TagViewModel, position: Int)
}