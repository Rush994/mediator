package com.plans.rush.mediator.data.network.source

import com.plans.rush.mediator.data.network.api.ImgurApi
import com.plans.rush.mediator.data.network.api.response.*
import com.plans.rush.mediator.data.network.response.PostResp
import com.plans.rush.mediator.presentation.base.types.TagTypes
import io.reactivex.Single
import javax.inject.Inject

class PostsNetSource @Inject constructor(private val api: ImgurApi): IPostsNetSource {

    override fun getPostListTop(page: Int): Single<PostListResp> =
        api.getBestPostList(page)
    override fun getPostListCat(page: Int): Single<PostListByTagResp> =
        api.getPostListByTag(TagTypes.Cat.value, page)
    override fun getPostListDog(page: Int): Single<PostListByTagResp> =
        api.getPostListByTag(TagTypes.Dog.value, page)
    override fun getPostListByTag(name: String, page: Int): Single<PostListByTagResp> =
        api.getPostListByTag(name, page)

    override fun getPostInner(postId: String): Single<PostSingleResp> =
        api.getPostInfoInner(postId)

    override fun getCommentList(postId: String): Single<CommentListResp> =
        api.getPostCommentList(postId)

    override fun getTagList(): Single<TagListDataResp> =
        api.getTagList()
}