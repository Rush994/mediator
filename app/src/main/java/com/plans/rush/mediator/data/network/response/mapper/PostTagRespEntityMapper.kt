package com.plans.rush.mediator.data.network.response.mapper

import com.plans.rush.mediator.data.network.response.TagResp
import com.plans.rush.mediator.domain.common.ListMapper
import com.plans.rush.mediator.domain.entity.TagEntity
import javax.inject.Inject

class PostTagRespEntityMapper @Inject constructor(): ListMapper<TagResp, TagEntity> {
    override fun mapFrom(item: TagResp): TagEntity =
        TagEntity(
            name = item.name,
            displayName = item.displayName,
            followers = item.followers,
            totalItems = item.totalItems
        )

    override fun mapFrom(list: List<TagResp>): List<TagEntity> =
        list.map{ mapFrom(it) }
}