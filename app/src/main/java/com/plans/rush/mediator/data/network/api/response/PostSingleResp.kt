package com.plans.rush.mediator.data.network.api.response

import com.google.gson.annotations.SerializedName
import com.plans.rush.mediator.data.network.response.ImageResp
import com.plans.rush.mediator.data.network.response.PostResp

data class PostSingleResp(
    @SerializedName("data")
    val data: PostResp,
    @SerializedName("success")
    val success: Boolean,
    @SerializedName("status")
    val status: Int
)