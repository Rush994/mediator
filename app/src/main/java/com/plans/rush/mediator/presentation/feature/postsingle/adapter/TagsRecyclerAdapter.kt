package com.plans.rush.mediator.presentation.feature.postsingle.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.plans.rush.mediator.R
import com.plans.rush.mediator.presentation.feature.postsingle.listener.OnTagClickListener
import com.plans.rush.mediator.presentation.viewmodel.TagViewModel
import kotlinx.android.synthetic.main.tag_list_item.view.*

class TagsRecyclerAdapter(
    private val tags: List<TagViewModel>,
    private val listener: (tag: String) -> Unit
) : RecyclerView.Adapter<TagsRecyclerAdapter.SingleItemHolder>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): SingleItemHolder =
        SingleItemHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.post_tag_item,
                parent,
                false
            )
        )

    override fun getItemCount(): Int = tags.size

    override fun onBindViewHolder(holder: SingleItemHolder, position: Int) {
        val item = tags.get(position)
        holder.bind(item)
    }

    inner class SingleItemHolder(private val view: View) : RecyclerView.ViewHolder(view) {
        fun bind(item: TagViewModel) {
            with(view) {
                chipTagItem.text = item.displayName
                setOnClickListener {
                    listener.invoke(item.name)
                }
            }
        }
    }
}