package com.plans.rush.mediator.data.repository

import android.content.Context
import com.plans.rush.mediator.data.db.model.ImageModel
import com.plans.rush.mediator.data.db.model.mapper.CommentModelEntityMapper
import com.plans.rush.mediator.data.db.model.mapper.ImageModelEntityMapper
import com.plans.rush.mediator.data.db.model.mapper.PostModelEntityMapper
import com.plans.rush.mediator.data.db.model.mapper.TagModelEntityMapper
import com.plans.rush.mediator.data.db.source.IPostsDBSource
import com.plans.rush.mediator.data.local.source.IPostsLocalSource
import com.plans.rush.mediator.data.network.response.PostResp
import com.plans.rush.mediator.data.network.response.mapper.CommentRespModelMapper
import com.plans.rush.mediator.data.network.response.mapper.PostRespModelMapper
import com.plans.rush.mediator.data.network.response.mapper.PostTagRespEntityMapper
import com.plans.rush.mediator.data.network.source.IPostsNetSource
import com.plans.rush.mediator.domain.entity.*
import com.plans.rush.mediator.domain.irepository.IPostsRepository
import com.plans.rush.mediator.presentation.base.types.CategoryTypes
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single
import javax.inject.Inject

class PostsRepository @Inject constructor(
    private val postNetSource: IPostsNetSource,
    private val postDBSource: IPostsDBSource,
    private val postsLocalSource: IPostsLocalSource,
    private val postModelEntityMapper: PostModelEntityMapper,
    private val postRespModelMapper: PostRespModelMapper,
    private val tagModelEntityMapper: TagModelEntityMapper,
    private val imageModelEntityMapper: ImageModelEntityMapper,
    private val commentRespModelMapper: CommentRespModelMapper,
    private val commentModelEntityMapper: CommentModelEntityMapper,
    private val postTagRespEntityMapper: PostTagRespEntityMapper
) : IPostsRepository {

    override fun loadPost(postId: String, categoryType: Int): Completable =
        postNetSource.getPostInner(postId)
            .map {
                postRespModelMapper.mapTo(it.data, categoryType)
            }
            .flatMapCompletable {
                postDBSource.insertPosts(listOf(it))
            }

    override fun findPost(postId: String): Flowable<PostEntity> =
        postDBSource.observePost(postId)
            .map {
                postModelEntityMapper.mapFrom(it)
            }

    override fun loadPostsDependsOnType(
        name: String,
        page: Int,
        postType: Int
    ): Single<List<PostResp>> =
        when (postType) {
            CategoryTypes.Ordinal.value -> postNetSource.getPostListTop(page).flatMap {
                Single.just(it.data)
            }
            CategoryTypes.Cat.value -> postNetSource.getPostListCat(page).flatMap {
                Single.just(it.data.items)
            }
            CategoryTypes.Dog.value -> postNetSource.getPostListDog(page).flatMap {
                Single.just(it.data.items)
            }
            CategoryTypes.ByTag.value -> postNetSource.getPostListByTag(name, page).flatMap {
                Single.just(it.data.items)
            }
            CategoryTypes.DeepLink.value, CategoryTypes.Default.value -> TODO()
            else -> TODO()
        }

    override fun observePosts(postType: Int): Flowable<List<PostEntity>> =
        postDBSource.observePosts(postType)
            .map {
                postModelEntityMapper.mapFrom(it)
            }

    override fun loadPosts(name: String, page: Int, postType: Int): Completable =
        loadPostsDependsOnType(name, page, postType)
            .map {
                postRespModelMapper.mapTo(it, postType)
            }
            .flatMap {
                if (page == 1)
                    postDBSource.deletePosts(postType).toSingle { it }
                else
                    Single.just(it)
            }
            .flatMapCompletable {
                postDBSource.insertPosts(it)
            }


    override fun observeImages(postId: String): Flowable<List<ImageEntity>> =
        postDBSource.observeImages(postId)
            .map {
                imageModelEntityMapper.mapFrom(it)
            }

    override fun observeJointPost(postId: String): Flowable<JointPostEntity> =
        postDBSource.observeComments(postId)
            .map {
                commentModelEntityMapper.mapFrom(it)
            }
            .flatMap { comments ->
                observeTags(postId)
                    .map {
                        JointPostEntity(comments = comments, tags = it)
                    }
            }

    override fun observeJointPostDeepLink(
        postId: String,
        categoryType: CategoryTypes
    ): Flowable<JointPostEntity> =
        postDBSource.observeComments(postId)
            .map {
                commentModelEntityMapper.mapFrom(it)
            }
            .flatMap { comments ->
                when (categoryType) {
                    CategoryTypes.Ordinal -> observeTags(postId)
                        .map {
                            JointPostEntity(comments = comments, tags = it)
                        }
                    CategoryTypes.DeepLink ->
                        findPost(postId)
                            .flatMap { post ->
                                observeTags(postId)
                                    .map { tags ->
                                        JointPostEntity(
                                            comments = comments,
                                            tags = tags,
                                            post = post
                                        )
                                    }
                            }
                    CategoryTypes.Default, CategoryTypes.Cat, CategoryTypes.Dog,
                    CategoryTypes.ByTag -> TODO()
                }
            }

    override fun observeTags(
        postId: String
    ): Flowable<List<TagEntity>> =
        postDBSource.observeTags(postId)
            .map { tagModelEntityMapper.mapFrom(it) }

    override fun insertImage(post: PostEntity): Completable =
        postDBSource.insertImage(
            ImageModel(
                post.id, post.mediaId, post.title, null, post.datetime, null, post.link
            )
        )

    override fun loadImages(postId: String, postType: Int): Completable =
        postNetSource.getPostInner(postId)
            .map {
                postRespModelMapper.mapTo(it.data, postType)
            }
            .flatMapCompletable {
                postDBSource.insertImages(it)
            }

    override fun loadComments(postId: String): Completable =
        postNetSource.getCommentList(postId) // "I57tpA2"
            .map {
                commentRespModelMapper.mapTo(it.data, postId)
            }
            .flatMapCompletable {
                postDBSource.insertComments(it)
            }


    override fun deletePostsByTag(): Completable =
        postDBSource.deletePostsByTag()

    override fun loadTags(): Single<List<TagEntity>> =
        postNetSource.getTagList()
            .map {
                postTagRespEntityMapper.mapFrom(it.data.tags)
            }

    override fun mediaDownload(
        folderPath: String,
        filePath: String,
        mediaUri: String,
        context: Context
    ): Completable =
        postsLocalSource.mediaDownload(folderPath, filePath, mediaUri, context)
}