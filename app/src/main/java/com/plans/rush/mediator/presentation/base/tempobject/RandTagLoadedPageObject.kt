package com.plans.rush.mediator.presentation.base.tempobject

data class RandTagLoadedPageObject(
    val name: String,
    val page: Int
)