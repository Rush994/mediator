package com.plans.rush.mediator.presentation.feature.postsingle.viewmodel

import android.content.Context
import android.os.Environment
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.plans.rush.mediator.domain.entity.PostEntity
import com.plans.rush.mediator.domain.interactor.*
import com.plans.rush.mediator.domain.usecase.*
import com.plans.rush.mediator.domain.usecase.postinner.*
import com.plans.rush.mediator.presentation.base.BaseViewModel
import com.plans.rush.mediator.presentation.base.tempobject.StateObject
import com.plans.rush.mediator.presentation.base.types.CategoryTypes
import com.plans.rush.mediator.presentation.viewmodel.JointPostViewModel
import com.plans.rush.mediator.presentation.viewmodel.ImageViewModel
import com.plans.rush.mediator.presentation.viewmodel.mapper.CommentsAndTagsEntityVMMapper
import com.plans.rush.mediator.presentation.viewmodel.mapper.ImageEntityVMMapper
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import retrofit2.HttpException
import java.io.File
import java.text.SimpleDateFormat
import java.util.*
import com.plans.rush.mediator.presentation.viewmodel.PostViewModel


class PostSingleViewModel constructor(
    private val postLoaders: PostLoaders,
    private val postObservers: PostObservers,
    private val commentLoaders: CommentLoaders,
    private val imageLoaders: ImageLoaders,
    private val insertCoverImageUseCase: InsertCoverImageUseCase,
    private val imageEntityVMMapper: ImageEntityVMMapper,
    private val commentsAndTagsEntityVMMapper: CommentsAndTagsEntityVMMapper,
    private val mediaDownloadUseCase: MediaDownloadUseCase
) : BaseViewModel() {

    var isMp4 = false
    var isDeepLink = false
    lateinit var post: PostViewModel
    lateinit var mediaUri: String
    lateinit var folderPath: String
    lateinit var filePath: String
    var appName: String = ""
        set(value) {
            field = value
            val fileName = "${SimpleDateFormat("dd.MM.yyyy", Locale.US).format(Date())}_${post.id}"
            folderPath =
                "${Environment.getExternalStorageDirectory()}${File.separator}${value}DownloadedMedia${File.separator}"
            filePath = "$folderPath$fileName"
        }

    val observedImagesLiveData = MutableLiveData<List<ImageViewModel>>()
    val observedCommentsLiveData = MutableLiveData<JointPostViewModel>()
    val statesLiveData = MutableLiveData<StateObject>()

    fun selectObservedSource() {
        when (post.categoryType) {
            CategoryTypes.Ordinal -> observePostOrdinal()
            CategoryTypes.DeepLink -> observePostDeepLink()
            CategoryTypes.Default, CategoryTypes.ByTag,
            CategoryTypes.Cat, CategoryTypes.Dog -> TODO()
        }
    }

    private fun observePostOrdinal() {
        postObservers.images(post.id)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .map { imageEntityVMMapper.mapFrom(it) }
            .subscribe({
                if (it.isNotEmpty())
                    observedImagesLiveData.value = it
            },
                {
                    if (it is HttpException) {
                        val resp = it.response()?.errorBody()
                    }
                    Log.d("Not get: ", it.toString())
                }).also { disposables.add(it) }

        postObservers.jointPost(post.id, CategoryTypes.Ordinal)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .map { commentsAndTagsEntityVMMapper.mapFrom(it) }
            .subscribe({
                observedCommentsLiveData.value = it
            },
                {
                    if (it is HttpException) {
                        val resp = it.response()?.errorBody()
                    }
                    Log.d("Not get: ", it.toString())
                }).also { disposables.add(it) }

        if (post.isAlbum)
            imageLoaders.topImages(post.id, CategoryTypes.Ordinal.value)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({},
                    {
                        if (it is HttpException) {
                            val resp = it.response()?.errorBody()
                            Log.d("Not get(http): ", it.toString())
                        } else {
                            Log.d("Not get(java): ", it.toString())
                        }
                    })
                .also { disposables.add(it) }
        else
            coverImageInsertion()

        commentLoaders.topComments(post.id)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({},
                {
                    if (it is HttpException) {
                        val resp = it.response()?.errorBody()
                        Log.d("Not get(http): ", it.toString())
                    } else {
                        Log.d("Not get(java): ", it.toString())
                    }
                })
            .also { disposables.add(it) }
    }

    private fun observePostDeepLink() {
        postObservers.images(post.id)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .map { imageEntityVMMapper.mapFrom(it) }
            .subscribe({
                if (it.isNotEmpty()) {
                    observedImagesLiveData.value = it
                }
            },
                {
                    if (it is HttpException) {
                        val resp = it.response()?.errorBody()
                    }
                    Log.d("Not get: ", it.toString())
                }).also { disposables.add(it) }

        postObservers.jointPost(post.id, CategoryTypes.DeepLink)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .map { commentsAndTagsEntityVMMapper.mapFrom(it) }
            .subscribe({
                if (it.comments.isNotEmpty()) {
                    isDeepLink = true
                    observedCommentsLiveData.value = it
                }
            },
                {
                    if (it is HttpException) {
                        val resp = it.response()?.errorBody()
                    }
                    Log.d("Not get: ", it.toString())
                }).also { disposables.add(it) }

        postLoaders.deepPost(post.id)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({},
                {
                    if (it is HttpException) {
                        val resp = it.response()?.errorBody()
                        Log.d("Not get(http): ", it.toString())
                    } else {
                        Log.d("Not get(java): ", it.toString())
                    }
                })
            .also { disposables.add(it) }
        commentLoaders.topComments(post.id)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {},
                {
                    if (it is HttpException) {
                        val resp = it.response()?.errorBody()
                        Log.d("Not get(http): ", it.toString())
                    } else {
                        Log.d("Not get(java): ", it.toString())
                    }
                }
            )
            .also { disposables.add(it) }
    }

    private fun coverImageInsertion() {
        insertCoverImageUseCase(
            PostEntity(
                id = post.id,
                mediaId = post.mediaId,
                title = post.title,
                datetime = post.datetime,
                cover = post.cover,
                owner = post.owner,
                views = post.views,
                link = post.link,
                thumbUps = post.thumbUps,
                thumbDowns = post.thumbDowns,
                commentCount = post.commentCount,
                isAlbum = post.isAlbum,
                mediaCount = post.mediaCount,
                categoryType = post.categoryType
            )
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({},
                {
                    if (it is HttpException) {
                        val resp = it.response()?.errorBody()
                        Log.d("Not get(http): ", it.toString())
                    } else {
                        Log.d("Not get(java): ", it.toString())
                    }
                })
            .also { disposables.add(it) }
    }

    fun isSDCardPresent(): Boolean {
        return Environment.getExternalStorageState() ==
                Environment.MEDIA_MOUNTED
    }

    fun mediaDownloading(sURI: String, context: Context) {
        filePath = "$filePath.${sURI.reversed().substringBefore('.').reversed()}"
        mediaDownloadUseCase(folderPath, filePath, sURI, context)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                statesLiveData.value = StateObject().apply {
                    progressVisible = true
                    completed = false
                }
            }
            .subscribe({
                statesLiveData.value = StateObject().apply {
                    progressVisible = false
                    completed = true
                }
            }, {
                if (it is HttpException) {
                    val resp = it.response()?.errorBody()
                    Log.d("Not get(http): ", it.toString())
                } else Log.d("Not get(java): ", it.toString())
            }).also { disposables.add(it) }
    }
}
