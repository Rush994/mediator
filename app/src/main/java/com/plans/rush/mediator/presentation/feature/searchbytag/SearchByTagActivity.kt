package com.plans.rush.mediator.presentation.feature.searchbytag

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import com.plans.rush.mediator.R
import com.plans.rush.mediator.presentation.base.BaseActivity
import com.plans.rush.mediator.presentation.base.Constants
import com.plans.rush.mediator.presentation.base.types.PageTypes
import com.plans.rush.mediator.presentation.feature.searchbytag.adapter.TagsRecyclerAdapter
import com.plans.rush.mediator.presentation.feature.searchbytag.listener.OnTagClickListener
import com.plans.rush.mediator.presentation.feature.searchbytag.viewmodel.SearchViewModel
import com.plans.rush.mediator.presentation.viewmodel.TagViewModel
import kotlinx.android.synthetic.main.activity_search.*
import javax.inject.Inject

class SearchByTagActivity : BaseActivity(), OnTagClickListener {

    @Inject
    lateinit var viewModel: SearchViewModel
    @Inject
    lateinit var layoutManager: GridLayoutManager
    @Inject
    lateinit var tagsAdapter: TagsRecyclerAdapter
    private var menu: Menu? = null
    private lateinit var menuItem: MenuItem

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search)
        supportActionBar?.setTitle(R.string.actionBarTitleSearch)

        rvTags.adapter = tagsAdapter
        rvTags.layoutManager = layoutManager
        viewModel.observedTagListLiveData.observe(
            this,
            Observer {
                tagsAdapter.setTagList(it)
            }
        )

        viewModel.tagClickedLiveData.observe(
            this,
            Observer { item ->
                menu?.let {
                    if (item == 0) {
                        menuItem.setEnabled(false)
                        menuItem.icon.alpha = Constants.ALPHA_NO_CHECKED_TAG
                    } else {
                        menuItem.setEnabled(true)
                        menuItem.icon.alpha = Constants.ALPHA_CHEKED_TAG
                    }
                }
            }
        )

        viewModel.loadTagList()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        this.menu = menu
        inflater.inflate(R.menu.action_bar_search_menu, menu)
        menuItem = menu.findItem(R.id.action_arrow)
        menuItem.isEnabled = false
        menuItem.icon.alpha = Constants.ALPHA_NO_CHECKED_TAG
        return true
    }

    override fun onItemClick(item: TagViewModel, position: Int) {
        var clickedCounter = viewModel.tagClickedLiveData.value!!
        val prevSelectedObject = tagsAdapter.discardSelection(clickedCounter)
        prevSelectedObject.clickedCounter?.let {
            clickedCounter = it
        }
        if(prevSelectedObject.discardedPosition == position)
            clickedCounter++
        if (prevSelectedObject.discardedPosition == position || item.isChecked){
            item.isChecked = false
            clickedCounter--
        } else {
            item.isChecked = true
            clickedCounter++
        }
        viewModel.tagClickedLiveData.value = clickedCounter
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean =
        when (item.itemId) {
            R.id.action_arrow -> {
                fireActivity(PageTypes.SearchByTag, tags = tagsAdapter.findCheckedItems())
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
}
