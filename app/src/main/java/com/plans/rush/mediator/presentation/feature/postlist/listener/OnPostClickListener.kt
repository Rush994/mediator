package com.plans.rush.mediator.presentation.feature.postlist.listener

import com.plans.rush.mediator.presentation.viewmodel.PostViewModel

interface OnPostClickListener {
    fun onItemClick(item: PostViewModel)
}