package com.plans.rush.mediator.presentation.feature.searchbytag.viewmodel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.plans.rush.mediator.domain.usecase.LoadTagListUseCase
import com.plans.rush.mediator.presentation.base.BaseViewModel
import com.plans.rush.mediator.presentation.viewmodel.TagViewModel
import com.plans.rush.mediator.presentation.viewmodel.mapper.TagEntityVMMapper
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import retrofit2.HttpException

class SearchViewModel(
    private val loadTagListUseCase: LoadTagListUseCase,
    private val tagEntityVMMapper: TagEntityVMMapper
): BaseViewModel() {

    val observedTagListLiveData = MutableLiveData<List<TagViewModel>>()
    var tagClickedLiveData = MutableLiveData<Int>()

    init {
        tagClickedLiveData.value = 0
    }

    fun loadTagList() =
        loadTagListUseCase()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .map { tagEntityVMMapper.mapFrom(it) }
            .subscribe({
                observedTagListLiveData.value = it
            },
                {
                    if (it is HttpException) {
                        val resp = it.response()?.errorBody()
                    }
                    Log.d("Not get: ", it.toString())
                }).also { disposables.add(it) }
}