package com.plans.rush.mediator.domain.common

interface ListMapper <in E, T> {
    fun mapFrom(item: E): T
    fun mapFrom(list: List<E>): List<T>
}