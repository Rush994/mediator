package com.plans.rush.mediator.data.db.dao

import androidx.room.Dao
import androidx.room.Query
import com.plans.rush.mediator.data.db.AppDatabase
import com.plans.rush.mediator.data.db.model.CommentModel
import io.reactivex.Flowable
import io.reactivex.Observable

@Dao
abstract class CommentsDao: BaseDao<CommentModel>(AppDatabase.TABLE_COMMENTS_TOP) {
    @Query("SELECT * FROM CommentsTop WHERE PostId LIKE :postId")
    abstract fun observeCommentList(postId: String): Flowable<List<CommentModel>>
}