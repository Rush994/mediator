package com.plans.rush.mediator.data.network.api.response

import com.google.gson.annotations.SerializedName

data class TagListDataResp(
    @SerializedName("data")
    val data: TagListResp,
    @SerializedName("success")
    val success: Boolean,
    @SerializedName("status")
    private var status: Int
)