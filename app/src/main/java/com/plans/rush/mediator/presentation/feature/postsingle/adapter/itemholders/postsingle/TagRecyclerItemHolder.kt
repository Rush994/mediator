package com.plans.rush.mediator.presentation.feature.postsingle.adapter.itemholders.postsingle

import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.plans.rush.mediator.presentation.feature.postsingle.adapter.TagsRecyclerAdapter
import com.plans.rush.mediator.presentation.feature.postsingle.listener.OnTagClickListener
import com.plans.rush.mediator.presentation.viewmodel.TagViewModel
import kotlinx.android.synthetic.main.post_single_tagrecycler_item.view.*

class TagRecyclerItemHolder(
    private val view: View,
    private val listener: (tag: String) -> Unit
) : RecyclerView.ViewHolder(view) {
    fun bind(item: List<TagViewModel>, viewPool: RecyclerView.RecycledViewPool) {
        view.rvPostTags.apply {
            layoutManager =
                LinearLayoutManager(view.rvPostTags.context, RecyclerView.HORIZONTAL, false)
            adapter = TagsRecyclerAdapter(item, listener)
            setRecycledViewPool(viewPool)
        }
    }
}