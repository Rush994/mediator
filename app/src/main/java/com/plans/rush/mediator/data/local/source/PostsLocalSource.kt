package com.plans.rush.mediator.data.local.source

import android.content.Context
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.plans.rush.mediator.presentation.base.Constants
import io.reactivex.Completable
import java.io.BufferedInputStream
import java.io.File
import java.io.FileOutputStream
import java.net.URL
import javax.inject.Inject


class PostsLocalSource @Inject constructor(): IPostsLocalSource {
    override fun mediaDownload(folderPath:String, filePath: String, mediaUri: String, context: Context): Completable =
        Completable.fromAction {
            val notificationManagerCompat = NotificationManagerCompat.from(context)

            val nDownloadBuilder: NotificationCompat.Builder = NotificationCompat.Builder(context,"downloader_channel")
            nDownloadBuilder.setTicker("Downloading")
            nDownloadBuilder.setOngoing(true)
            nDownloadBuilder.setAutoCancel(true)
            nDownloadBuilder.setSmallIcon(android.R.drawable.stat_sys_download)
            nDownloadBuilder.setContentTitle("Downloading")
            nDownloadBuilder.setContentText("0%")
            nDownloadBuilder.setProgress(100, 0, false)
            notificationManagerCompat.notify(Constants.DOWNLOAD_NOTIFICATION_ID, nDownloadBuilder.build())

            ///////////////////////////////////
            val url = URL(mediaUri)
            val connection = url.openConnection()
            connection.connect()
            val lengthOfFile = connection.contentLength
            val input = BufferedInputStream(url.openStream(), 8192)
            //Create folder if it does not exist
            val directory = File(folderPath)
            if (!directory.exists()) {
                directory.mkdirs()
            }
            // create a new file
            val output = FileOutputStream(filePath)

            val data = ByteArray(1024)
            var total: Long = 0
            var count: Int = 0
            var tmpPercentage: Int = 0
            while (count != -1) {
                total += count.toLong()
                output.write(data, 0, count)

                val percentage = (total * 100 / lengthOfFile).toInt()
                Log.d(Constants.DOWLOADING, "Progress: $percentage")
                if (percentage > tmpPercentage) {
                    nDownloadBuilder.setContentText("$percentage%")
                    nDownloadBuilder.setProgress(100, percentage, false)
                    notificationManagerCompat.notify(
                        Constants.DOWNLOAD_NOTIFICATION_ID,
                        nDownloadBuilder.build()
                    )
                    tmpPercentage = percentage
                }
                count = input.read(data)
            }
            notificationManagerCompat.cancel(Constants.DOWNLOAD_NOTIFICATION_ID)
            // flushing output
            output.flush()
            // closing streams
            output.close()
            input.close()
        }

}