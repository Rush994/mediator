package com.plans.rush.mediator.data.network.response

import com.google.gson.annotations.SerializedName

data class PostResp(
    @SerializedName("id")
    val id: String,
    @SerializedName("title")
    val title: String,
    @SerializedName("datetime")
    val datetime: Long,
    @SerializedName("cover")
    val cover: String,
    @SerializedName("account_url")
    val owner: String,
    @SerializedName("views")
    val views: Int,
    @SerializedName("link")
    val link: String,
    @SerializedName("ups")
    val thumbUps: Int,
    @SerializedName("downs")
    val thumbDowns: Int,
    @SerializedName("comment_count")
    val commentCount: Int,
    @SerializedName("is_album")
    val isAlbum: Boolean,
    @SerializedName("images_count")
    val mediaCount: Int?,
    @SerializedName("tags")
    val tags: List<TagResp>,
    @SerializedName("images")
    val images: List<ImageResp>?
)