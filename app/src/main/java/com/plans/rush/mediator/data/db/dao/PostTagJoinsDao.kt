package com.plans.rush.mediator.data.db.dao

import androidx.room.Dao
import androidx.room.Query
import com.plans.rush.mediator.data.db.AppDatabase
import com.plans.rush.mediator.data.db.model.TagModel
import com.plans.rush.mediator.data.db.model.PostTagJoinModel
import io.reactivex.Flowable

@Dao
abstract class PostTagJoinsDao: BaseDao<PostTagJoinModel>(AppDatabase.TABLE_POST_TAG_JOINS_TOP){
    @Query("""SELECT * FROM PostTags INNER JOIN PostTagJoinsTop ON
                    PostTags.Id=PostTagJoinsTop.tagId WHERE
                    PostTagJoinsTop.postId LIKE :postId""")
    abstract fun observePostTags(postId: String): Flowable<List<TagModel>>
}