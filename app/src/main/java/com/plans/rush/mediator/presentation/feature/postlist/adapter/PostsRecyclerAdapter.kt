package com.plans.rush.mediator.presentation.feature.postlist.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.plans.rush.mediator.R
import com.plans.rush.mediator.presentation.feature.postlist.listener.OnPostClickListener
import com.plans.rush.mediator.presentation.viewmodel.PostViewModel
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.post_list_item.view.*

class PostsRecyclerAdapter(val listener: OnPostClickListener) :
    RecyclerView.Adapter<PostsRecyclerAdapter.SingleItemHolder>() {

    var posts: List<PostViewModel> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = SingleItemHolder(
        LayoutInflater.from(parent.context).inflate(
            R.layout.post_list_item, parent, false))

    override fun getItemCount(): Int = posts.size

    override fun onBindViewHolder(holder: SingleItemHolder, position: Int) {
        val item = posts[position]
        holder.bind(item)
    }

    inner class SingleItemHolder(private val view: View) : RecyclerView.ViewHolder(view) {

        fun bind(item: PostViewModel) {
            with(view){
                if (item.mediaCount == null)
                    item.mediaCount = 1
                if (item.mediaCount != 1)
                {
                    with(tvMediasCount)
                    {
                        text = item.mediaCount.toString()
                        visibility = View.VISIBLE
                    }
                }
                else
                {
                    tvMediasCount.visibility = View.GONE
                }
                tvPostHeader.text = item.title
                Picasso
                    .with(context)
                    .load("https://i.imgur.com/${item.mediaId}.jpg")
                    .placeholder(R.drawable.ic_default_picture)
                    .into(ivPostPhoto)
                setOnClickListener{
                    listener.onItemClick(item)
                }
            }
        }
    }
}