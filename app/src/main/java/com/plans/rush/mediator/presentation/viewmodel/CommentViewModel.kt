package com.plans.rush.mediator.presentation.viewmodel

data class CommentViewModel(
    val id: Long,
    val imageId: String,
    val comment: String,
    val author: String,
    val ups: Long,
    val downs: Long,
    val platform: String
)