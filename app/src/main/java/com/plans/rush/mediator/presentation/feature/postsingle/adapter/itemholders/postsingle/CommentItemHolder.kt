package com.plans.rush.mediator.presentation.feature.postsingle.adapter.itemholders.postsingle

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.amulyakhare.textdrawable.TextDrawable
import com.amulyakhare.textdrawable.util.ColorGenerator
import com.plans.rush.mediator.R
import com.plans.rush.mediator.presentation.viewmodel.CommentViewModel
import kotlinx.android.synthetic.main.post_single_comment_item.view.*

class CommentItemHolder(private val view: View) : RecyclerView.ViewHolder(view) {

    val materialColors: ArrayList<Int> = arrayListOf(
        R.color.material_Color1,
        R.color.material_Color2,
        R.color.material_Color3,
        R.color.material_Color4,
        R.color.material_Color5,
        R.color.material_Color6,
        R.color.material_Color7,
        R.color.material_Color8,
        R.color.material_Color9,
        R.color.material_Color10,
        R.color.material_Color11,
        R.color.material_Color12,
        R.color.material_Color13,
        R.color.material_Color14,
        R.color.material_Color15,
        R.color.material_Color16,
        R.color.material_Color17
    )
    val materialGenerator = ColorGenerator.MATERIAL
    lateinit var roundDrawable: TextDrawable
    fun bind(item: CommentViewModel) {
        with(view) {
            tvCommentAutor.text = item.author
            when (item.platform) {
                "iphone" -> ivCommentPlatform.setImageResource(R.drawable.ic_apple)
                "android" -> ivCommentPlatform.setImageResource(R.drawable.ic_android)
                "desktop" -> ivCommentPlatform.setImageResource(R.drawable.ic_desktop)
                "api" -> ivCommentPlatform.setImageResource(R.drawable.ic_api)
            }
            tvCommentText.text = item.comment
            tvCommentThumsUp.text = item.ups.toString()
            tvCommentThumsDown.text = item.downs.toString()
            roundDrawable = TextDrawable.builder()
                .buildRound(item.author.get(0).toString(), materialGenerator.randomColor)
            ivCommentAuthorImage.setImageDrawable(roundDrawable)
            //DrawableCompat.setTint(ivCommentAuthorImage.background,ContextCompat.getColor(context, materialColors.get(rndColor)));
        }
    }
}