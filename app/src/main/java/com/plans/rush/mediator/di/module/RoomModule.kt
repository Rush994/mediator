package com.plans.rush.mediator.di.module

import android.content.Context
import androidx.room.Room
import com.plans.rush.mediator.R
import com.plans.rush.mediator.data.db.AppDatabase
import com.plans.rush.mediator.data.db.dao.*
import com.plans.rush.mediator.di.scope.PerApplication
import dagger.Module
import dagger.Provides

@Module
class RoomModule {

    @PerApplication
    @Provides
    fun providesRoomDatabase(mContext: Context): AppDatabase =
        Room.databaseBuilder(mContext, AppDatabase::class.java, "${mContext.getString(R.string.app_name)}DB").build()

    @PerApplication
    @Provides
    fun providePostsOuterBestDao(appDatabase: AppDatabase): PostsDao =
        appDatabase.getPostsOutersDao()

    @PerApplication
    @Provides
    fun providePostInnersBestDao(appDatabase: AppDatabase): ImagesDao =
        appDatabase.getPostInnersDao()

    @PerApplication
    @Provides
    fun provideCommentsBestDao(appDatabase: AppDatabase): CommentsDao =
        appDatabase.getCommentsDao()

    @PerApplication
    @Provides
    fun providePostTagsDao(appDatabase: AppDatabase): TagsDao =
        appDatabase.getPostTagsDao()

    @PerApplication
    @Provides
    fun providePostTagJoinsBestDao(appDatabase: AppDatabase): PostTagJoinsDao =
        appDatabase.getPostTagJoinsDao()
}