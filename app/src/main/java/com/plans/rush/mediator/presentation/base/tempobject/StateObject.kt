package com.plans.rush.mediator.presentation.base.tempobject

data class StateObject(
    var progressVisible: Boolean = false,
    var errorText: String? = null,
    var completed: Boolean = false
)