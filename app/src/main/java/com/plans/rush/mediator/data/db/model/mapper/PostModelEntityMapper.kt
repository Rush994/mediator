package com.plans.rush.mediator.data.db.model.mapper

import com.plans.rush.mediator.data.db.model.PostModel
import com.plans.rush.mediator.domain.entity.PostEntity
import com.plans.rush.mediator.presentation.base.types.CategoryTypes
import javax.inject.Inject

class PostModelEntityMapper @Inject constructor() {
    fun mapFrom(item: PostModel): PostEntity =
        PostEntity(
            id = item.id,
            mediaId = item.mediaId,
            title = item.title,
            datetime = item.datetime,
            cover = item.cover,
            owner = item.owner,
            views = item.views,
            link = item.link,
            thumbUps = item.thumbUps,
            thumbDowns = item.thumbDowns,
            commentCount = item.commentCount,
            isAlbum = item.isAlbum,
            mediaCount = item.mediaCount,
            categoryType = CategoryTypes.Ordinal
        )


    fun mapFrom(list: List<PostModel>): List<PostEntity> =
            list.map { mapFrom(it) }

    @Suppress("UNCHECKED_CAST")
    fun mapFrom(list: List<Any>, item: Any): List<PostEntity>{
        var mappedList: List<PostEntity> = listOf()
        if (item is PostModel)
        {
            list as List<PostModel>
            mappedList = list.map { mapFrom(it) }
        }

        return mappedList
    }
}