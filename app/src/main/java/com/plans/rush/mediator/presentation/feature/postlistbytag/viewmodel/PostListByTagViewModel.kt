package com.plans.rush.mediator.presentation.feature.postlistbytag.viewmodel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.google.gson.Gson
import com.plans.rush.mediator.data.network.api.response.ErrorResp
import com.plans.rush.mediator.domain.interactor.PostLoaders
import com.plans.rush.mediator.domain.interactor.PostObservers
import com.plans.rush.mediator.domain.usecase.postouter.DeletePostsByTagUseCase
import com.plans.rush.mediator.presentation.base.BaseViewModel
import com.plans.rush.mediator.presentation.base.tempobject.RandTagLoadedPageObject
import com.plans.rush.mediator.presentation.base.tempobject.StateObject
import com.plans.rush.mediator.presentation.base.types.CategoryTypes
import com.plans.rush.mediator.presentation.viewmodel.PostViewModel
import com.plans.rush.mediator.presentation.viewmodel.mapper.PostOuterEntityVMMapper
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.processors.PublishProcessor
import io.reactivex.schedulers.Schedulers
import retrofit2.HttpException

class PostListByTagViewModel(
    private val postLoaders: PostLoaders,
    private val postObservers: PostObservers,
    private val deletePostsByTagUseCase: DeletePostsByTagUseCase,
    private val postOuterEntityVMMapper: PostOuterEntityVMMapper
): BaseViewModel() {

    val postListLiveData = MutableLiveData<List<PostViewModel>>()
    val progressStateLiveData = MutableLiveData<StateObject>()

    var lastLoadedPage = 0
    private var loadPaginator = PublishProcessor.create<RandTagLoadedPageObject>()
    var loading = false

    fun postListInit(tagArray: ArrayList<String>) {
        lastLoadedPage = 0
        initObserver()
        initLoader()
        android.os.Handler().postDelayed({
            loadPosts(tagArray)
        }, 100)
    }

    private fun initObserver(): Disposable? =
        postObservers.posts(CategoryTypes.ByTag.value)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .map { postOuterEntityVMMapper.mapFrom(it) }
            .subscribe({
                if (!it.isEmpty()) {
                    postListLiveData.value = it
                    progressStateLiveData.value = StateObject(
                        progressVisible = false,
                        completed = true
                    )
                    loading = false
                }
            },
                {
                    if (it is HttpException) {
                        val resp = it.response()?.errorBody()
                    }
                    Log.d("Not get: ", it.toString())
                })
            .also { disposables.add(it) }

    private fun initLoader() =
        loadPaginator
            .concatMapCompletable {
                postLoaders.topPosts(it.name, it.page, CategoryTypes.ByTag.value)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
            }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe {
                progressStateLiveData.value = StateObject(
                    progressVisible = true,
                    completed = false
                )
            }
            .subscribe({
            },
                {
                    if (it is HttpException) {
                        val gson = Gson()
                        val resp = it.response()?.errorBody()
                        val errorObject = gson.fromJson(resp.toString(), ErrorResp::class.java)
                        Log.d("Not get: ", errorObject.error)
                    }
                    Log.d("Not get: ", it.toString())
                }).also { disposables.add(it) }

    fun loadPosts(tagArray: ArrayList<String>) {
        lastLoadedPage++
        loadPaginator.onNext(RandTagLoadedPageObject(tagArray[0], lastLoadedPage))
    }


    fun deletePostsByTag() =
        deletePostsByTagUseCase()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({}, {
                if (it is HttpException) {
                    val resp = it.response()?.errorBody()
                    Log.d("Not get(http): ", it.toString())
                } else Log.d("Not get(java): ", it.toString())
            }).also { disposables.add(it) }
}