package com.plans.rush.mediator.data.network.api.response

import com.google.gson.annotations.SerializedName
import com.plans.rush.mediator.data.network.response.TagResp

data class TagListResp(
    @SerializedName("tags")
    val tags: List<TagResp>,
    @SerializedName("featured")
    val featured: String
)