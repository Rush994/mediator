package com.plans.rush.mediator.data.db.dao

import androidx.room.Dao
import androidx.room.Query
import com.plans.rush.mediator.data.db.AppDatabase
import com.plans.rush.mediator.data.db.model.PostModel
import io.reactivex.Flowable

@Dao
abstract class PostsDao: BaseDao<PostModel>(AppDatabase.TABLE_POSTS_TOP) {
    @Query("SELECT * FROM PostsTop WHERE PostType=:postType")
    abstract fun observeBestPostList(postType: Int): Flowable<List<PostModel>>

    @Query("SELECT * FROM PostsTop WHERE Id LIKE :postId")
    abstract fun findPost(postId: String): Flowable<PostModel>

    @Query("DELETE FROM PostsTop WHERE PostType=:postType")
    abstract fun deletePostList(postType: Int)

}