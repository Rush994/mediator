package com.plans.rush.mediator.data.network.api.response

import com.google.gson.annotations.SerializedName
import com.plans.rush.mediator.data.network.response.CommentResp

data class CommentListResp(
    @SerializedName("data")
    val data: List<CommentResp>,
    @SerializedName("success")
    val success: Boolean,
    @SerializedName("status")
    private var status: Int
)