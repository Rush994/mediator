package com.plans.rush.mediator.domain.usecase.postouter

import com.plans.rush.mediator.domain.irepository.IPostsRepository
import io.reactivex.Completable
import javax.inject.Inject

class DeletePostsByTagUseCase @Inject constructor(
    private val postsRepository: IPostsRepository
) {
    operator fun invoke(): Completable =
        postsRepository.deletePostsByTag()
}