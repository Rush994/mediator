package com.plans.rush.mediator.presentation.viewmodel

data class ImageViewModel(
    val postId: String,
    val mediaId: String,
    val title: String?,
    val description: String?,
    val datetime: Long,
    val mp4: String?,
    var link: String
)