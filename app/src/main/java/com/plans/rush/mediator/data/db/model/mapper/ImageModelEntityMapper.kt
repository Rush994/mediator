package com.plans.rush.mediator.data.db.model.mapper

import com.plans.rush.mediator.data.db.model.ImageModel
import com.plans.rush.mediator.domain.entity.ImageEntity
import javax.inject.Inject

class ImageModelEntityMapper @Inject constructor(){
    //region Ordinal
    fun mapFrom(item: ImageModel): ImageEntity =
        ImageEntity(
            postId = item.postId,
            mediaId = item.mediaId,
            title = item.title,
            description = item.description,
            datetime = item.datetime,
            mp4 = item.mp4,
            link = item.link
        )
    fun mapFrom(list: List<ImageModel>): List<ImageEntity> =
        list.map { mapFrom(it) }
    //endregion
}