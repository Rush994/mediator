package com.plans.rush.mediator.presentation.feature.postlist.adapter

import androidx.recyclerview.widget.DiffUtil
import com.plans.rush.mediator.presentation.viewmodel.PostViewModel

class PostsDiffUtil: DiffUtil.Callback() {
    var oldList: List<PostViewModel> = listOf()
    var newList: List<PostViewModel> = listOf()

    override fun getOldListSize(): Int = oldList.size
    override fun getNewListSize(): Int = newList.size
    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean =
        oldList[oldItemPosition].id == newList[newItemPosition].id
    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean =
        oldList[oldItemPosition] == newList[newItemPosition]
}