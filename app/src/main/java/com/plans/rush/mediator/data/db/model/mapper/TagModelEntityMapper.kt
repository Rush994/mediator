package com.plans.rush.mediator.data.db.model.mapper

import com.plans.rush.mediator.data.db.model.TagModel
import com.plans.rush.mediator.domain.common.ListMapper
import com.plans.rush.mediator.domain.entity.TagEntity
import javax.inject.Inject

class TagModelEntityMapper @Inject constructor(): ListMapper<TagModel, TagEntity> {
    override fun mapFrom(item: TagModel): TagEntity =
        TagEntity(
            name = item.name,
            displayName = item.displayName,
            followers = item.followers,
            totalItems = item.totalItems
        )

    override fun mapFrom(list: List<TagModel>): List<TagEntity> =
        list.map { mapFrom(it) }

}