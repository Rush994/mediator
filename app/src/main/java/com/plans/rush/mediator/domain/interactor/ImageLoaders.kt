package com.plans.rush.mediator.domain.interactor

import com.plans.rush.mediator.domain.usecase.postinner.LoadImagesUseCase
import javax.inject.Inject

class ImageLoaders @Inject constructor(
    private val loadImagesUseCase: LoadImagesUseCase
    ) {
    fun topImages(postId: String, postType: Int) =
        loadImagesUseCase(postId, postType)
}