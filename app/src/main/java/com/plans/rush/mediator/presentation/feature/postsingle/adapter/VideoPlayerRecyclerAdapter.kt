package com.plans.rush.mediator.presentation.feature.postsingle.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.RequestManager
import com.plans.rush.mediator.R
import com.plans.rush.mediator.presentation.feature.postsingle.adapter.itemholders.media.VideoPlayerViewHolder
import com.plans.rush.mediator.presentation.viewmodel.ImageViewModel
import java.util.ArrayList

class VideoPlayerRecyclerAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val mediaObjects: ArrayList<ImageViewModel> = arrayListOf()
    lateinit var requestManager: RequestManager

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return VideoPlayerViewHolder(
            LayoutInflater.from(parent.getContext()).inflate(
                R.layout.post_single_media_item,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int = mediaObjects.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as VideoPlayerViewHolder).bind(mediaObjects[position], requestManager)
    }

}