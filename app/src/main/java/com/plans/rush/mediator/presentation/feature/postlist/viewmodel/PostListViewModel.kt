package com.plans.rush.mediator.presentation.feature.postlist.viewmodel

import androidx.lifecycle.MutableLiveData
import com.plans.rush.mediator.presentation.base.BaseViewModel
import com.plans.rush.mediator.presentation.base.types.CategoryTypes
import com.plans.rush.mediator.presentation.feature.plcat.PLCatFragment
import com.plans.rush.mediator.presentation.feature.pldog.PLDogFragment
import com.plans.rush.mediator.presentation.feature.pltop.PLTopFragment
import com.plans.rush.mediator.presentation.feature.postlist.FragmentNavigationManager

class PostListViewModel constructor(
    private val fragmentNavigator: FragmentNavigationManager
) : BaseViewModel() {

    val titleChangeLiveData = MutableLiveData<String>()
    lateinit var currentCategoryType: CategoryTypes

    fun openTopTab(title: String): Boolean
    {
        fragmentNavigator.navigateTo(PLTopFragment.newInstance())
        titleChangeLiveData.value = title
        currentCategoryType = CategoryTypes.Ordinal
        return true
    }
    fun openCatTab(title: String): Boolean
    {
        fragmentNavigator.navigateTo(PLCatFragment.newInstance())
        titleChangeLiveData.value = title
        currentCategoryType = CategoryTypes.Cat
        return true
    }
    fun openDogTab(title: String): Boolean
    {
        fragmentNavigator.navigateTo(PLDogFragment.newInstance())
        titleChangeLiveData.value = title
        currentCategoryType = CategoryTypes.Dog
        return true
    }
}