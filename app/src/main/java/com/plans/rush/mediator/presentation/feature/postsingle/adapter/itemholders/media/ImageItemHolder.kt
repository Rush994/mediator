package com.plans.rush.mediator.presentation.feature.postsingle.adapter.itemholders.media

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.plans.rush.mediator.R
import com.plans.rush.mediator.presentation.base.types.ContextMenuItemTypes
import com.plans.rush.mediator.presentation.viewmodel.ImageViewModel
import kotlinx.android.synthetic.main.media_image_item.view.*

class ImageItemHolder(private val view: View) : RecyclerView.ViewHolder(view) {
    fun bind(item: ImageViewModel, position: Int){
        with(view) {
            Glide
                .with(this)
                .load("https://i.imgur.com/${item.mediaId}.jpg")
                .fitCenter()
                .placeholder(R.drawable.ic_default_picture)
                .into(imageView);
            imageDescription.text = item.description
            setOnCreateContextMenuListener { contextMenu, view, contextMenuInfo ->
                contextMenu.add(position, ContextMenuItemTypes.Download.value,
                    0, R.string.postInfoDownload)
                contextMenu.add(position, ContextMenuItemTypes.Share.value,
                    0, R.string.postInfoShare)
            }
        }
    }
}