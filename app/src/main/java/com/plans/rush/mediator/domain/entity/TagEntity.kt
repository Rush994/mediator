package com.plans.rush.mediator.domain.entity

data class TagEntity(
    val name: String,
    val displayName: String,
    val followers: Int,
    val totalItems: Int
)