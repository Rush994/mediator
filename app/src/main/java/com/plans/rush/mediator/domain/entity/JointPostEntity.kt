package com.plans.rush.mediator.domain.entity

data class JointPostEntity(
    var post: PostEntity = PostEntity(),
    var comments: List<CommentEntity> = listOf(),
    var tags: List<TagEntity> = listOf()
)