package com.plans.rush.mediator.data.db.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import com.plans.rush.mediator.data.db.AppDatabase

@Entity(
    tableName = AppDatabase.TABLE_POST_TAG_JOINS_TOP,
    primaryKeys = (arrayOf("postId", "tagId")),
    foreignKeys = [
        ForeignKey(
            entity = PostModel::class,
            parentColumns = arrayOf("Id"),
            childColumns = arrayOf("postId"),
            onDelete = ForeignKey.CASCADE
        ),
        ForeignKey(
            entity = TagModel::class,
            parentColumns = arrayOf("Id"),
            childColumns = arrayOf("tagId"),
            onDelete = ForeignKey.CASCADE
        )])
data class PostTagJoinModel(
    var postId: String,
    @ColumnInfo(index = true)
    var tagId: Long
)