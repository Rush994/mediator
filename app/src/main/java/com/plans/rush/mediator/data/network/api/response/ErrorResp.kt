package com.plans.rush.mediator.data.network.api.response

import com.google.gson.annotations.SerializedName

data class ErrorResp(
    @SerializedName("error")
    val error: String,
    @SerializedName("request")
    val request: String,
    @SerializedName("method")
    val method: String
)