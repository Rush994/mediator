package com.plans.rush.mediator.data.network.api.response

import com.google.gson.annotations.SerializedName
import com.plans.rush.mediator.data.network.response.PostListByTagDataResp

data class PostListByTagResp(
    @SerializedName("data")
    val data: PostListByTagDataResp,
    @SerializedName("success")
    val success: Boolean,
    @SerializedName("status")
    val status: Int
)