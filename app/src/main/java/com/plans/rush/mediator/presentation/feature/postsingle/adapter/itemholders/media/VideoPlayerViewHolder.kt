package com.plans.rush.mediator.presentation.feature.postsingle.adapter.itemholders.media

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.RequestManager
import com.google.android.exoplayer2.ui.SimpleExoPlayerView
import com.plans.rush.mediator.presentation.viewmodel.ImageViewModel
import kotlinx.android.synthetic.main.post_single_media_item.view.*

class VideoPlayerViewHolder(private val view: View) : RecyclerView.ViewHolder(view) {

    val parent: View
    val simpleExoPlayer: SimpleExoPlayerView
    val image: ImageView
    val videoDescriptionTV: TextView
    lateinit var requestManager: RequestManager

    init {
        parent = view
        simpleExoPlayer = view.simpleExoPlayerView
        image = view.imageView
        videoDescriptionTV = view.videoDescription
    }

    fun bind(mediaObject: ImageViewModel, requestManager: RequestManager) {
        this.requestManager = requestManager
        parent.tag = this
        videoDescriptionTV.setText(mediaObject.description)
    }
}