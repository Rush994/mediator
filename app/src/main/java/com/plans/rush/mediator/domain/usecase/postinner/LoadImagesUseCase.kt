package com.plans.rush.mediator.domain.usecase.postinner

import com.plans.rush.mediator.domain.irepository.IPostsRepository
import io.reactivex.Completable
import javax.inject.Inject

class LoadImagesUseCase @Inject constructor(
    private val postsRepository: IPostsRepository
){
    operator fun invoke(postId: String, postType: Int): Completable =
        postsRepository.loadImages(postId, postType)
}