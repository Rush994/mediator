package com.plans.rush.mediator.di.module

import com.plans.rush.mediator.BuildConfig
import com.plans.rush.mediator.data.network.api.ImgurApi
import com.plans.rush.mediator.data.network.api.ImgurApi.Companion.BASE_URL
import com.plans.rush.mediator.data.network.interceptor.RequestInterceptor
import com.plans.rush.mediator.di.scope.PerApplication
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

@Module
class NetworkModule {
    @PerApplication
    @Provides
    fun provideRetrofit(requestInterceptor: RequestInterceptor): Retrofit {

        val httpClient = OkHttpClient.Builder()

        httpClient.addInterceptor(HttpLoggingInterceptor().apply {
            level = if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE })
            .addInterceptor(requestInterceptor)

        return Retrofit.Builder()
            .client(httpClient.build())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(BASE_URL)
            .build()
    }

    @PerApplication
    @Provides
    fun provideApi(retrofit: Retrofit): ImgurApi =
        retrofit.create(ImgurApi::class.java)
}