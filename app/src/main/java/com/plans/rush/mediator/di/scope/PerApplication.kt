package com.plans.rush.mediator.di.scope

import javax.inject.Scope

@MustBeDocumented
@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class PerApplication