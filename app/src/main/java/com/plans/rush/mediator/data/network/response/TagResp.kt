package com.plans.rush.mediator.data.network.response

import com.google.gson.annotations.SerializedName

data class TagResp(
    @SerializedName("name")
    val name: String,
    @SerializedName("display_name")
    val displayName: String,
    @SerializedName("followers")
    val followers: Int,
    @SerializedName("total_items")
    val totalItems: Int

)