package com.plans.rush.mediator.domain.usecase

import android.content.Context
import com.plans.rush.mediator.domain.irepository.IPostsRepository
import io.reactivex.Completable
import javax.inject.Inject

class MediaDownloadUseCase @Inject constructor(
    private val postsRepository: IPostsRepository
) {
    operator fun invoke(folderPath:String, filePath: String, mediaUri: String, context: Context): Completable =
        postsRepository.mediaDownload(folderPath, filePath, mediaUri, context)
}